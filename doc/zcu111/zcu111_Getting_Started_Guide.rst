.. zcu111_Getting_Started_Guide OpenCPI ZCU111 Getting Started Guide documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _zcu111_Getting_Started_Guide:


OpenCPI ZCU111 Getting Started Guide
====================================

This document provides steps for configuring a factory provided Xilinx ``zcu111`` 
with the OpenCPI run-time environment for executing applications.

Use this guide as a companion to the *OpenCPI Installation Guide*. The installation guide is 
required since many of the steps mentioned here are defined there, especially in the 
section: Enabling OpenCPI Development for Embedded Systems. This document provides details 
for this system that can be applied to procedures defined there. It is best to use both 
documents at the same time. This document assumes a basic understanding of the Linux 
command line (or ``shell``) environment. The reference(s) below can be used as an 
overview of OpenCPI and may prove useful. 

**The zcu111 project contains the "Guide for developing an OpenCPI Board Support Package 
(OSP) - Case Study zcu111" and should be referenced for additional setup and installation.**

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/v2.1.1/docs/OpenCPI_User_Guide.pdf>`_
  
* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/v2.1.1/docs/OpenCPI_Glossary.pdf>`_

* `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/v2.1.1/docs/OpenCPI_Installation_Guide.pdf>`_

Note that the *OpenCPI Glossary* is also contained in both the *OpenCPI Installation Guide* and the
*OpenCPI User Guide*.

Revision History
----------------

.. csv-table:: OpenCPI ZCU111 Getting Started Guide: Revision History
   :header: "Revision", "Description of Change", "Date"
   :widths: 10,30,10
   :class: tight-table

   "v2.1", "Initial Release", "7/2021"

Bug Fix to the framework
------------------------

The following bug fix MUST be applied to the v2.1.0 framework release to address an intermittent 
run-time failure observed on the embedded system. The implementation of this bug fix is required 
PRIOR to installing (i.e. cross-compiling) the OpenCPI run-time utilities for any targeted embedded 
Software RCC Platform, such as, the ``xilinx19_2_aarch64``.

::

    $ git diff runtime/dataplane/transport/src/OcpiInputBuffer.cxx
    WARNING: terminal is not fully functional
    -  (press RETURN) 
    diff --git a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx b/runtime/dataplane/transport
    /src/OcpiInputBuffer.cxx
    index 3d37c6e..03276cd 100644
    --- a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
    +++ b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
    @@ -147,7 +147,7 @@ void InputBuffer::update(bool critical)
            sizeof(BufferMetaData)*MAX_PCONTRIBS);
 
         memset(m_bmdVaddr,0,sizeof(BufferMetaData)*MAX_PCONTRIBS);
    -    getPort()->getEndPoint().doneWithInput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
    +    getPort()->getEndPoint().doneWithOutput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
         m_sbMd = static_cast<volatile BufferMetaData (*)[MAX_PCONTRIBS]>(m_bmdVaddr);
       }

Software Prerequisites
----------------------

It is assumed that the tasks defined in the `Enabling OpenCPI Development for Embedded Systems` section 
of the *OpenCPI Installation Guide* has been successfully performed. Support for the ``zcu111`` system is 
located in the ``ocpi.osp.xilinx`` project, targeting the ``zcu111`` OpenCPI HDL (FPGA) platform and the 
``xilinx19_2_aarch64`` OpenCPI RCC (software) platform. The RCC platforms are supported by the ``ocpi.core`` 
built-in project.


RCC Platforms
~~~~~~~~~~~~~~~~~

The RCC platform ``xilinx19_2_aarch64`` is required by the ``zcu111`` variation.

For successful installation and deployment of ``xilinx19_2_aarch64`` RCC platform, take special note in the 
*OpenCPI Installation Guide* steps that outline the implementation of the ZynqReleases directory and git 
directory. The ZynqReleases directory and git directory will be placed in the Xilinx tools installation directory 
(/tools/Xilinx or /opt/Xilinx).

As a convenience, a ``2019.2-zcu111-release.tar.xz`` file is available in the platforms directory of the ``zcu111``. 
These files can be found in the ``ocpi.osp.xilinx`` repository and can be placed into the ZynqReleases directory 
as a Linux Prebuilt Image.

**File Location**: /home/user/ocpi.osp.xilinx/hdl/platforms/zcu111


Third-party/Vendor Tools
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Also indicated in the  *OpenCPI Installation Guide*, are the tools used for software cross-compilation from 
the Xilinx Vitis 2019.2. A full license of Vivado is needed for FPGA development . The Linux Kernel is based
on the Xilinx Linux kernel tagged ``xilinx_v2019.2`` (for ``xilinx19_2_aarch64``). The installation of these
tools is described in the installation guide.

Build the Required OpenCPI Projects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The standard built-in OpenCPI projects are built using the instructions in the *OpenCPI Installation Guide*. 
This results in a single test executable application ``testbias`` based on the ``testbias`` HDL assembly 
(FPGA bitstream), which are both in the ``ocpi.assets`` built-in project.

Setup and Build summary
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Below is an abbreviated set of steps that will guide the user through the installation and deployment of the ``zcu111``
hdl-platform and the ``xilinx19_2_aarch64`` rcc-platform.

The steps provided below rely heavily on the *OpenCPI Installation Guide*. For additional information, reference
the ”Guide for developing an OpenCPI Board Support Package (OSP) - Case Study zcu111” at
ocpi.osp.xilinx/guide/zcu111/Guide.md

The values of the ”Duration” in the steps below are approximated and directly dependent on the system resources
of the development host. They are roughly based on a development host having an Intel Xeon 3.90GHz x 16 with
128GB RAM.

#. Clone and Install the OpenCPI framework. (Duration 30 min)

   $ cd /home/user

   $ git clone https://gitlab.com/opencpi/opencpi.git

   $ cd opencpi

   $ git checkout tags/v2.1.0

   $ ./scripts/install-opencpi.sh

#. Install for the zcu104 HDL Platform (Duration < 14 hours)

   This step is a workaround for an issue in the framework, where the framework does
   not properly install an HDL Platform that is located in the projects/osp/.
   Therefore, the user is instructed to install for a built-in HDL platform that
   targets the same Xilinx device family. This will ensure that all of the HDL assets
   are built for the correct target device. Once all of the HDL assets are built,
   then the target zcu111 HDL platform can be built, and finally the zcu111
   platform can be installed and deployed.

   $ ocpiadmin install platform zcu104

#. Clone ocpi.osp.xilinx project into the appropriate directory

   $ cd /home/user/opencpi/projects/osps

   $ git clone <location>/ocpi.osp.xilinx.git

#. Register the ocpi.osp.xilinx project

   $ cd ocpi.osp.xilinx/

   $ ocpidev register project

#. Implement the fmc_plus.xml card slot spec file and fmcomms_2_3_plus_scdcd.xml card

   The zcu111 has one FMC+ card slot that can be used to connect plug-in modules or daughtercards
   and is backwards compatable with LPC and HPC daughtercards. However, the FMC+ card slot is not
   currently supported by the framework and in order to utilize the FMC+ card slot and the FMCOMMS
   devices the FMC+ card slot spec will need to be implemented into the framework along with the
   fmcomms_2_3_plus_scdcd.xml card. Provided in the zcu111 repository is the fmc_plus.xml card slot
   spec file. Outlined below are the steps for implementing the fmc_plus.xml card slot spec file
   and fmcomms_2_3_plus_scdcd.xml card.

   #. Copy the fmc_plus.xml card slot spec into the appropriate directory

      $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/guide/zcu111

      $ cp fmc_plus.xml /home/user/opencpi/projects/core/hdl/cards/specs

   #. Once the card spec has been copied, clean the core exports

      $ cd /home/user/opencpi/projects/core

      $ make cleanexports

   #. Update the core exports which makes it visible to the build system

      $ make exports

   #. Copy the fmcomms_2_3_lpc_scdcd.xml and rename it fmcomms_2_3_plus_scdcd.xml

      $ cd /home/user/opencpi/projects/assets/hdl/cards/specs

      $ cp fmcomms_2_3_lpc_scdcd.xml fmcomms_2_3_plus_scdcd.xml

   #. Edit the newly created fmcomms_2_3_plus_scdcd.xml and change the ’card type’ from "fmc_lpc" to "fmc_plus" 

      <card type="fmc_plus">

   #. Once the card has been created, clean the assets exports

      $ cd /home/user/opencpi/projects/assets

      $ make cleanexports

   #. Update the assets exports which makes it visible to the build system

      $ make exports

#. Build for the zcu111 target platform (Duration < 40 min)

   $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx

   $ ocpidev build --hdl-platform zcu111

#. Install HDL platform (Duration < 20 min)

   $ cd /home/user/opencpi

   $ ocpiadmin install platform zcu111

#. Setup the ZynqReleases directory

   Using the steps described in the ``OpenCPI Installation Guide`` or ocpi.osp.xilinx/guide/zcu111/Guide.md,
   setup the ZynqReleases directory (/tools/Xilinx/ZynqReleases or /opt/Xilinx/ZynqReleases) 
   Copy the zcu111 provided boot artifacts 2019.2-zcu111-release.tar.xz file that contains the 
   ``BOOT.BIN`` and ``image.ub``.
   
   Pay close attention to setting the permissions on the created directory and its contents.

#. Setup the git directory

   Using the steps described in the ``OpenCPI Installation Guide``, setup the git directory
   (/tools/Xilinx/git or /opt/Xilinx/git) for cross-compiling the OpenCPI run-time tools for
   xilinx19_2_aarch64. Pay close attention to setting the permissions on the created directory 
   and its contents.

#. Install RCC platform (Duration < 10 min). Be sure that the **Bug Fix to the Framework** has been implemented.

   $ ocpiadmin install platform xilinx19_2_aarch64

#. Deploy HDL platform with RCC platform

   $ ocpiadmin deploy platform xilinx19_2_aarch64 zcu111

Hardware Prerequisites
----------------------

Xilinx zcu111 Product Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Xilinx zcu111 product package should include the following items:

* Power Supply

* micro-USB to USB cable

* Micro-USB to female-USB adapter

* Standard SD card (16GB)

UART
~~~~

Micro-USB serial port located on the top-side of the zcu111 labeled USB JTAG UART, can 
be used to access the serial connection with the processor. 

.. image:: ./figures/zcu111_uart.jpg
   :alt: *Figure 1: Connected Serial USB*
   :align: center
   :width: 800

SD Card Slot
~~~~~~~~~~~~

The SD card slot is locate next to the power-switch and micro-USB serial port. The SD card
slot will be used throughout this guide.

.. image:: ./figures/zcu111_sd.jpg
   :alt: *Figure 2: SD Card Slot*
   :align: center
   :width: 800

Ethernet cable
~~~~~~~~~~~~~~

An Ethernet port is available on the zcu111 and is required when the Network or Server Mode is
used. The OpenCPI BSP for the zcu111 is configured for DHCP.

.. image:: ./figures/zcu111_ether.jpg
   :alt: *Figure 3: Connected Ethernet Cable*
   :align: center
   :width: 800

OpenCPI zcu111 Supported Daughtercards (Optional)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Currently, OpenCPI supports two FMC daughtercards, which may be installed on the zcu111:
   - Analog Devices FMCOMMS2
   - Analog Devices FMCOMMS3

The zcu111 has an FMC+ slot that is used to connect plug-in modules or daughtercards. The user
should reference the Guide.md for additional setup that is required for using the OpenCPI supported
daughtercards.

.. image:: ./figures/zcu111_daughter_card.jpg
   :alt: *Figure 4: Installed Analog Devices fmcomms2 daughtercard*
   :align: center
   :width: 800

DHCP Support
~~~~~~~~~~~~

Using the zcu111 in Network or SErver Mode requires access to a network that supports DHCP.

SD Card Reader
~~~~~~~~~~~~~~

The zcu111 hardware setup requires an SD card reader.

Setting up the SD Card
----------------------

The chapter "Enableing OpenCPI Development for Embedded Systems" in the *OpenCPI Installation Guide* provides
a section "Installation Steps for Systems after its Platfroms are installed" that describes how to create a 
new SD card for OpenCPI and how to customize some of the OpenCPI SD card files for your particular configuration.
The recommended method is to make a raw copy of the manufacturer-supplied card to a new card, preserving formatting
and content, and then remove most of the original files and copy files from OpenCPI. If you need to format the SD
card for the zcu11 system, it should be a single FAT32 partition.

Once the zcu111 (hdl-platform) and Xilinx19 2 aarch64 (rcc-platform) have been successfully installed and deployed
in the **Setup and Build summary**, the following steps can be taken in order to create a valid SD-Card for the zcu111
system.

#. Create SD-card (single FAT32 partition) for installing in the zcu111

#. cp -RLp /home/user/opencpi/cdk/zcu111/sdcard-xilinx19_2_aarch64/* /run/media/<user>/<card>

Setting up Multiple zcu111’s on the same network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

By default, each zcu111 released by the factory has the same MAC address. If you require multiple zcu111's to be
on the same network, you must edit the SD card OpenCPI startup scripts ``mynetsetup.sh`` or ``mysetup.sh`` to 
provide a unique MAC address for each zcu111. These scripts are created for the zcu111 when you perform the step
described in the sections "Preparing the SD Card Contents" in the *OpenCPI Installation Guide*.

To update the SD card startup scripts, uncomment the following lines in the ``mynetsetup.sh`` and/or ``mysetup.sh``
scripts and then change the Ethernet address to be unique. For example:
::

   # ifconfig eth0 down
   # ifconfig eth0 hw ether 00:0a:35:00:01:24
   # ifconfig eth0 up
   # udhcpc

where ``00:0A:35:00:01:24`` is the unique MAC address for the zcu111

Setting up the Hardware
-----------------------

The *OpenCPI Installation Guide* describes the procedure to establish a serial console and boot the system. This section
provides zcu111-specific information that applies to these tasks.

Establish a Seral Console Connection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The *OpenCPI Installation Guide* has a section "Establishing a Serial Console connection" that describes the procedure
to establish a serial console. The zcu111 console serial port is configured for 115200 baud. The cable used is a micro-USB 
to USB-A cable to connect its console micro-USB port to the development host.

Booting the zcu111 from the SD card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The zcu111 unit needs to be configured to boot from the SD card. To configure the unit and then boot from the SD card:

#. Remove power from the zcu111 unit.

#. Ensure jumpers are configured correctly by consulting the Xilinx user-guide of the zcu111.

#. zcu111: Default the jumpers are set to boot from QSPI. However, in order to boot from a micro-SD card the jumpers need to be set according to image below.

.. image:: ./figures/zcu111_boot_jmp.jpg
   :alt: *Figure 5: Top view of the zcu111 boot jumper setting*
   :align: center
   :width: 800

#. With the contents provided in the **SD Card Setup** section, insert the SD-card into the SD-card slot.

#. Connect a terminal to the micro-USB connector labeled ’UART’ on the zcu111.

#. On the development host, start the terminal emulator (usually the ``screen`` command) at 115200 baud.

#. Apply power to the zcu111 with the terminal still connected.

Configuring the Runtime Enviornment on the Platform
---------------------------------------------------

The *OpenCPI Installation Guide* provides the procedure for setting up and verifying the run-time environment. This system is
initially set with **“root”** for user name and password.

After a successful boot to PetaLinux, login to the system, using **“root“** for user name and password.

Take note of the **root@plx zcu111 dp** indicating that the Control-Plane and Data-Plane of the zcu111 have
successfully booted using PetaLinux.

.. image:: ./figures/zcu111_boot.png
   :alt: *Figure 6: Successful Boot to Petalinux*
   :align: center
   :width: 1700

Server Mode Setup
~~~~~~~~~~~~~~~~~

**CRITICAL NOTE: These instructions for using Server Mode are not officially published, use them
with caution.**

**Server-side setup:**

#. Establish a serial connection from the Host to the zcu111, open a terminal window:

   $ sudo screen /dev/ttyUSB0 115200

#. Petalinux Login:

   plx_zcu111_dp login:root

   Password:root

#. Network setup:

  ::

   $ ifconfig eth0 down
   $ ifconfig eth0 add <Vailid remote ip-address> netmask 255.255.255.0
   $ ifconfig eth0 up

**Client-side setup:**

#. Change to your OpenCPI directory:

   $ cd /home/user/opencpi

#. Export some OpenCPI enviornment variables to discover the remote server:

   $ export OCPI_SERVER_ADDRESSES=<Valid ip-address>:<Valid port>

   $ export OCPI_SOCKET_INTERFACE=<Valid socket>

   Note: Item "Valid socket" is the name of the Ethernet interface of the development host

#. Load "sandbox" onto the server:

   $ ocpiremote load -s xilinx19_2_aarch64 -w zcu111

#. Start the Server-Mode with the DMA_CACHE_MODE disabled and load the default bitstream (testbias):

   $ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0

**Trouble Shooting**

When presented with the following error, check the log file in the sandbox directory on the remote
system to see what the error is:

::

    OCPI( 2:991.0828): Exception during application shutdown: error reading from container
    server "": EOF on socket read Exiting for exception: error reading from container
    server "": EOF on socket read

Sometimes it may mean that the user may need to disable or reconfigure the firewall
of the host system. You can disable your firewall until it is rebooted with the two
following commands:

$ sudo systemctl disable firewalld

$ sudo systemctl mask --now firewalld

Run an Application
------------------

With **Server Mode Setup** now enabled, the following steps will run the test application testbias, based on the testbias
HDL assembly, both in the assets built-in project.

#. On the Client host, navigate to the OpenCPI applications located in the assets built-in project

   $ cd /home/user/opencpi/projects/assets/applications

#. Setup the OCPI_LIBRARY_PATH

   $ export OCPI_LIBRARY_PATH=/home/user/opencpi/projects/core/artifacts:/home/user/opencpi/projects/assets/artifacts

#. Run the testbias application executing file_read and file_write on the host

   $ ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 -P file_read=centos7 -P file_write=centos7
   testbias.xml

#. Validate the output data by comparing it to the input data. When the biasvalue is 0 these files will have the same md5sum.

   $ md5sum test.input

   $ md5sum test.output
