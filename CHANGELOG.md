# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/v2.4.6...v2.4.7) (2024-01-10)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!13)(5682512d)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/v2.4.3...v2.4.4) (2023-01-22)

Changes/additions since [OpenCPI Release v2.4.3](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.3)

### New Features
- **osp**: add zcu111 support. (!11)(2c571a11)

# [v2.4.3](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/v2.4.2...v2.4.3) (2022-10-11)

Changes/additions since [OpenCPI Release v2.4.2](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.2)

### Bug Fixes
- **doc**: fix LaTeX documentation. (!7)(f5775697)
- **doc**: fix markdown error in OSP README. (!10)(a3d51cb7)

### Miscellaneous
- **doc**: create RST files to describe procedures for setting up ZCUxxx/ZCU102 platforms. (!9)(6ea848c8)
- **doc**: delete obsolete LaTeX getting started guide and entire doc directory. (!9)(6ea848c8)
- **doc**: update README.md to reference RST version of ZCU102 getting started guide. (!9)(6ea848c8)
- **doc**: update ZCU111 Getting Started Guide to OpenCPI "installation doc" best practices & V2.4. (!10)(a3d51cb7)
- **doc,osp**: delete redundant `snippets` directory. (!8)(83d91f44)

# [v2.4.2](https://gitlab.com/opencpi/osp/ocpi.osp.xilinx/-/compare/1c886af5...v2.4.2) (2022-05-26)

Initial release of xilinx OSP
