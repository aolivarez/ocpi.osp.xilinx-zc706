
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library zynq_ultra_zcu111; use zynq_ultra_zcu111.zynq_ultra_zcu111_pkg.all;
library axi; use axi.axi_pkg.all;
entity zynq_ultra_zcu111_ps is
  port(ps_in        : in    pl2ps_t;
       ps_out       : out   ps2pl_t;
       m_axi_hp_in  : in    axi.zynq_ultra_m_hp.axi_s2m_array_t(0 to C_M_AXI_HP_COUNT-1);
       m_axi_hp_out : out   axi.zynq_ultra_m_hp.axi_m2s_array_t(0 to C_M_AXI_HP_COUNT-1));
       --s_axi_hp_in  : in    axi.zynq_ultra_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
       --s_axi_hp_out : out   axi.zynq_ultra_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1));
end entity zynq_ultra_zcu111_ps;
architecture rtl of zynq_ultra_zcu111_ps is
  COMPONENT preset_config_wrapper_zynq_ultra_ps_e_0_0 is
    Port ( 
      maxihpm0_fpd_aclk : in STD_LOGIC;
      maxigp0_awid : out STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      maxigp0_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
      maxigp0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      maxigp0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      maxigp0_awlock : out STD_LOGIC;
      maxigp0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      maxigp0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      maxigp0_awvalid : out STD_LOGIC;
      maxigp0_awuser : out STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_awready : in STD_LOGIC;
      maxigp0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      maxigp0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      maxigp0_wlast : out STD_LOGIC;
      maxigp0_wvalid : out STD_LOGIC;
      maxigp0_wready : in STD_LOGIC;
      maxigp0_bid : in STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      maxigp0_bvalid : in STD_LOGIC;
      maxigp0_bready : out STD_LOGIC;
      maxigp0_arid : out STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      maxigp0_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
      maxigp0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      maxigp0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      maxigp0_arlock : out STD_LOGIC;
      maxigp0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      maxigp0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      maxigp0_arvalid : out STD_LOGIC;
      maxigp0_aruser : out STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_arready : in STD_LOGIC;
      maxigp0_rid : in STD_LOGIC_VECTOR ( 15 downto 0 );
      maxigp0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      maxigp0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      maxigp0_rlast : in STD_LOGIC;
      maxigp0_rvalid : in STD_LOGIC;
      maxigp0_rready : out STD_LOGIC;
      maxigp0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      maxigp0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      emio_uart1_txd : out STD_LOGIC;
      emio_uart1_rxd : in STD_LOGIC;
      pl_ps_irq0 : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_resetn0 : out STD_LOGIC;
      pl_clk0 : out STD_LOGIC
    );
  END COMPONENT preset_config_wrapper_zynq_ultra_ps_e_0_0;
begin
  U0 :  preset_config_wrapper_zynq_ultra_ps_e_0_0
    PORT MAP (
      maxihpm0_fpd_aclk => m_axi_hp_in(0).A.CLK,
      maxigp0_awid => m_axi_hp_out(0).AW.ID, --[15:0]
      maxigp0_awaddr => m_axi_hp_out(0).AW.ADDR, --[39:0]
      maxigp0_awlen => m_axi_hp_out(0).AW.LEN, --[7:0]
      maxigp0_awsize => m_axi_hp_out(0).AW.SIZE, --[2:0]
      maxigp0_awburst => m_axi_hp_out(0).AW.BURST, --[1:0]
      maxigp0_awlock => m_axi_hp_out(0).AW.LOCK(0),
      maxigp0_awcache => m_axi_hp_out(0).AW.CACHE, --[3:0]
      maxigp0_awprot => m_axi_hp_out(0).AW.PROT, --[2:0]
      maxigp0_awvalid => m_axi_hp_out(0).AW.VALID,
      maxigp0_awuser => open, --[15:0]
      maxigp0_awready => m_axi_hp_in(0).AW.READY,
      maxigp0_wdata => m_axi_hp_out(0).W.DATA, --[C_MAXIGP0_DATA_WIDTH-1 :0]
      maxigp0_wstrb => m_axi_hp_out(0).W.STRB, --[(C_MAXIGP0_DATA_WIDTH/8)-1 :0]
      maxigp0_wlast => m_axi_hp_out(0).W.LAST,
      maxigp0_wvalid => m_axi_hp_out(0).W.VALID,
      maxigp0_wready => m_axi_hp_in(0).W.READY,
      maxigp0_bid => m_axi_hp_in(0).B.ID, --[15:0]
      maxigp0_bresp => m_axi_hp_in(0).B.RESP, --[1:0]
      maxigp0_bvalid => m_axi_hp_in(0).B.VALID,
      maxigp0_bready => m_axi_hp_out(0).B.READY,
      maxigp0_arid => m_axi_hp_out(0).AR.ID, --[15:0]
      maxigp0_araddr => m_axi_hp_out(0).AR.ADDR, --[39:0]
      maxigp0_arlen => m_axi_hp_out(0).AR.LEN, --[7:0]
      maxigp0_arsize => m_axi_hp_out(0).AR.SIZE, --[2:0]
      maxigp0_arburst => m_axi_hp_out(0).AR.BURST, --[1:0]
      maxigp0_arlock => m_axi_hp_out(0).AR.LOCK(0),
      maxigp0_arcache => m_axi_hp_out(0).AR.CACHE, --[3:0]
      maxigp0_arprot => m_axi_hp_out(0).AR.PROT, --[2:0]
      maxigp0_arvalid => m_axi_hp_out(0).AR.VALID,
      maxigp0_aruser => open, --[15:0]
      maxigp0_arready => m_axi_hp_in(0).AR.READY,
      maxigp0_rid => m_axi_hp_in(0).R.ID, --[15:0]
      maxigp0_rdata => m_axi_hp_in(0).R.DATA, --[C_MAXIGP0_DATA_WIDTH-1 :0]
      maxigp0_rresp => m_axi_hp_in(0).R.RESP, --[1:0]
      maxigp0_rlast => m_axi_hp_in(0).R.LAST,
      maxigp0_rvalid => m_axi_hp_in(0).R.VALID,
      maxigp0_rready => m_axi_hp_out(0).R.READY,
      maxigp0_awqos => m_axi_hp_out(0).AW.QOS, --[3:0]
      maxigp0_arqos => m_axi_hp_out(0).AR.QOS, --[3:0]

      --saxihp0_fpd_aclk => s_axi_hp_in(0).A.CLK,

      --saxigp2_awid => s_axi_hp_in(0).AW.ID(5 downto 0), --[5:0]
      --saxigp2_awaddr => s_axi_hp_in(0).AW.ADDR, --[48:0]
      --saxigp2_awlen => s_axi_hp_in(0).AW.LEN, --[7:0]
      --saxigp2_awsize => s_axi_hp_in(0).AW.SIZE, --[2:0]
      --saxigp2_awburst => s_axi_hp_in(0).AW.BURST, --[1:0]
      --saxigp2_awlock => s_axi_hp_in(0).AW.LOCK(0),
      --saxigp2_awcache => s_axi_hp_in(0).AW.CACHE, --[3:0]
      --saxigp2_awprot => s_axi_hp_in(0).AW.PROT, --[2:0]
      --saxigp2_awvalid => s_axi_hp_in(0).AW.VALID,
      --saxigp2_awuser => '0', --[15:0]
      --saxigp2_awready => s_axi_hp_out(0).AW.READY,
      --saxigp2_wdata => s_axi_hp_in(0).W.DATA, --[C_SAXIGP2_DATA_WIDTH-1:0]
      --saxigp2_wstrb => s_axi_hp_in(0).W.STRB, --[(C_SAXIGP2_DATA_WIDTH/8) -1 :0]
      --saxigp2_wlast => s_axi_hp_in(0).W.LAST,
      --saxigp2_wvalid => s_axi_hp_in(0).W.VALID,
      --saxigp2_wready => s_axi_hp_out(0).W.READY,
      --saxigp2_bid => s_axi_hp_out(0).B.ID(5 downto 0), --[5:0]
      --saxigp2_bresp => s_axi_hp_out(0).B.RESP, --[1:0]
      --saxigp2_bvalid => s_axi_hp_out(0).B.VALID,
      --saxigp2_bready => s_axi_hp_in(0).B.READY,
      --saxigp2_arid => s_axi_hp_in(0).AR.ID(5 downto 0), --[5:0]
      --saxigp2_araddr => s_axi_hp_in(0).AR.ADDR, --[48:0]
      --saxigp2_arlen => s_axi_hp_in(0).AR.LEN, --[7:0]
      --saxigp2_arsize => s_axi_hp_in(0).AR.SIZE, --[2:0]
      --saxigp2_arburst => s_axi_hp_in(0).AR.BURST, --[1:0]
      --saxigp2_arlock => s_axi_hp_in(0).AR.LOCK(0),
      --saxigp2_arcache => s_axi_hp_in(0).AR.CACHE, --[3:0]
      --saxigp2_arprot => s_axi_hp_in(0).AR.PROT, --[2:0]
      --saxigp2_arvalid => s_axi_hp_in(0).AR.VALID,
      --saxigp2_aruser => '0', --[15:0]
      --saxigp2_arready => s_axi_hp_out(0).AR.READY,
      --saxigp2_rid => s_axi_hp_out(0).R.ID(5 downto 0), --[5:0]
      --saxigp2_rdata => s_axi_hp_out(0).R.DATA, --[C_SAXIGP2_DATA_WIDTH-1:0]
      --saxigp2_rresp => s_axi_hp_out(0).R.RESP, --[1:0]
      --saxigp2_rlast => s_axi_hp_out(0).R.LAST,
      --saxigp2_rvalid => s_axi_hp_out(0).R.VALID,
      --saxigp2_rready => s_axi_hp_in(0).R.READY,
      --saxigp2_awqos => s_axi_hp_in(0).AW.QOS, --[3:0]
      --saxigp2_arqos => s_axi_hp_in(0).AR.QOS, --[3:0]

      --saxihp1_fpd_aclk => s_axi_hp_in(1).A.CLK,

      --saxigp3_awid => s_axi_hp_in(1).AW.ID(5 downto 0), --[5:0]
      --saxigp3_awaddr => s_axi_hp_in(1).AW.ADDR, --[48:0]
      --saxigp3_awlen => s_axi_hp_in(1).AW.LEN, --[7:0]
      --saxigp3_awsize => s_axi_hp_in(1).AW.SIZE, --[2:0]
      --saxigp3_awburst => s_axi_hp_in(1).AW.BURST, --[1:0]
      --saxigp3_awlock => s_axi_hp_in(1).AW.LOCK(0),
      --saxigp3_awcache => s_axi_hp_in(1).AW.CACHE, --[3:0]
      --saxigp3_awprot => s_axi_hp_in(1).AW.PROT, --[2:0]
      --saxigp3_awvalid => s_axi_hp_in(1).AW.VALID,
      --saxigp3_awuser => '0', --[15:0]
      --saxigp3_awready => s_axi_hp_out(1).AW.READY,
      --saxigp3_wdata => s_axi_hp_in(1).W.DATA, --[C_SAXIGP3_DATA_WIDTH-1:0]
      --saxigp3_wstrb => s_axi_hp_in(1).W.STRB, --[(C_SAXIGP3_DATA_WIDTH/8) -1 :0]
      --saxigp3_wlast => s_axi_hp_in(1).W.LAST,
      --saxigp3_wvalid => s_axi_hp_in(1).W.VALID,
      --saxigp3_wready => s_axi_hp_out(1).W.READY,
      --saxigp3_bid => s_axi_hp_out(1).B.ID(5 downto 0), --[5:0]
      --saxigp3_bresp => s_axi_hp_out(1).B.RESP, --[1:0]
      --saxigp3_bvalid => s_axi_hp_out(1).B.VALID,
      --saxigp3_bready => s_axi_hp_in(1).B.READY,
      --saxigp3_arid => s_axi_hp_in(1).AR.ID(5 downto 0), --[5:0]
      --saxigp3_araddr => s_axi_hp_in(1).AR.ADDR, --[48:0]
      --saxigp3_arlen => s_axi_hp_in(1).AR.LEN, --[7:0]
      --saxigp3_arsize => s_axi_hp_in(1).AR.SIZE, --[2:0]
      --saxigp3_arburst => s_axi_hp_in(1).AR.BURST, --[1:0]
      --saxigp3_arlock => s_axi_hp_in(1).AR.LOCK(0),
      --saxigp3_arcache => s_axi_hp_in(1).AR.CACHE, --[3:0]
      --saxigp3_arprot => s_axi_hp_in(1).AR.PROT, --[2:0]
      --saxigp3_arvalid => s_axi_hp_in(1).AR.VALID,
      --saxigp3_aruser => '0', --[15:0]
      --saxigp3_arready => s_axi_hp_out(1).AR.READY,
      --saxigp3_rid => s_axi_hp_out(1).R.ID(5 downto 0), --[5:0]
      --saxigp3_rdata => s_axi_hp_out(1).R.DATA, --[C_SAXIGP3_DATA_WIDTH-1:0]
      --saxigp3_rresp => s_axi_hp_out(1).R.RESP, --[1:0]
      --saxigp3_rlast => s_axi_hp_out(1).R.LAST,
      --saxigp3_rvalid => s_axi_hp_out(1).R.VALID,
      --saxigp3_rready => s_axi_hp_in(1).R.READY,
      --saxigp3_awqos => s_axi_hp_in(1).AW.QOS, --[3:0]
      --saxigp3_arqos => s_axi_hp_in(1).AR.QOS, --[3:0]
      --
      --saxihp2_fpd_aclk => s_axi_hp_in(2).A.CLK,

      --saxigp4_awid => s_axi_hp_in(2).AW.ID(5 downto 0), --[5:0]
      --saxigp4_awaddr => s_axi_hp_in(2).AW.ADDR, --[48:0]
      --saxigp4_awlen => s_axi_hp_in(2).AW.LEN, --[7:0]
      --saxigp4_awsize => s_axi_hp_in(2).AW.SIZE, --[2:0]
      --saxigp4_awburst => s_axi_hp_in(2).AW.BURST, --[1:0]
      --saxigp4_awlock => s_axi_hp_in(2).AW.LOCK(0),
      --saxigp4_awcache => s_axi_hp_in(2).AW.CACHE, --[3:0]
      --saxigp4_awprot => s_axi_hp_in(2).AW.PROT, --[2:0]
      --saxigp4_awvalid => s_axi_hp_in(2).AW.VALID,
      --saxigp4_awuser => '0', --[15:0]
      --saxigp4_awready => s_axi_hp_out(2).AW.READY,
      --saxigp4_wdata => s_axi_hp_in(2).W.DATA, --[C_SAXIGP4_DATA_WIDTH-1:0]
      --saxigp4_wstrb => s_axi_hp_in(2).W.STRB, --[(C_SAXIGP4_DATA_WIDTH/8) -1 :0]
      --saxigp4_wlast => s_axi_hp_in(2).W.LAST,
      --saxigp4_wvalid => s_axi_hp_in(2).W.VALID,
      --saxigp4_wready => s_axi_hp_out(2).W.READY,
      --saxigp4_bid => s_axi_hp_out(2).B.ID(5 downto 0), --[5:0]
      --saxigp4_bresp => s_axi_hp_out(2).B.RESP, --[1:0]
      --saxigp4_bvalid => s_axi_hp_out(2).B.VALID,
      --saxigp4_bready => s_axi_hp_in(2).B.READY,
      --saxigp4_arid => s_axi_hp_in(2).AR.ID(5 downto 0), --[5:0]
      --saxigp4_araddr => s_axi_hp_in(2).AR.ADDR, --[48:0]
      --saxigp4_arlen => s_axi_hp_in(2).AR.LEN, --[7:0]
      --saxigp4_arsize => s_axi_hp_in(2).AR.SIZE, --[2:0]
      --saxigp4_arburst => s_axi_hp_in(2).AR.BURST, --[1:0]
      --saxigp4_arlock => s_axi_hp_in(2).AR.LOCK(0),
      --saxigp4_arcache => s_axi_hp_in(2).AR.CACHE, --[3:0]
      --saxigp4_arprot => s_axi_hp_in(2).AR.PROT, --[2:0]
      --saxigp4_arvalid => s_axi_hp_in(2).AR.VALID,
      --saxigp4_aruser => '0', --[15:0]
      --saxigp4_arready => s_axi_hp_out(2).AR.READY,
      --saxigp4_rid => s_axi_hp_out(2).R.ID(5 downto 0), --[5:0]
      --saxigp4_rdata => s_axi_hp_out(2).R.DATA, --[C_SAXIGP4_DATA_WIDTH-1:0]
      --saxigp4_rresp => s_axi_hp_out(2).R.RESP, --[1:0]
      --saxigp4_rlast => s_axi_hp_out(2).R.LAST,
      --saxigp4_rvalid => s_axi_hp_out(2).R.VALID,
      --saxigp4_rready => s_axi_hp_in(2).R.READY,
      --saxigp4_awqos => s_axi_hp_in(2).AW.QOS, --[3:0]
      --saxigp4_arqos => s_axi_hp_in(2).AR.QOS, --[3:0]
     
      --saxihp3_fpd_aclk => s_axi_hp_in(3).A.CLK,
      --
      --saxigp5_awid => s_axi_hp_in(3).AW.ID(5 downto 0), --[5:0]
      --saxigp5_awaddr => s_axi_hp_in(3).AW.ADDR, --[48:0]
      --saxigp5_awlen => s_axi_hp_in(3).AW.LEN, --[7:0]
      --saxigp5_awsize => s_axi_hp_in(3).AW.SIZE, --[2:0]
      --saxigp5_awburst => s_axi_hp_in(3).AW.BURST, --[1:0]
      --saxigp5_awlock => s_axi_hp_in(3).AW.LOCK(0),
      --saxigp5_awcache => s_axi_hp_in(3).AW.CACHE, --[3:0]
      --saxigp5_awprot => s_axi_hp_in(3).AW.PROT, --[2:0]
      --saxigp5_awvalid => s_axi_hp_in(3).AW.VALID,
      --saxigp5_awuser => '0', --[15:0]
      --saxigp5_awready => s_axi_hp_out(3).AW.READY,
      --saxigp5_wdata => s_axi_hp_in(3).W.DATA, --[C_SAXIGP5_DATA_WIDTH-1:0]
      --saxigp5_wstrb => s_axi_hp_in(3).W.STRB, --[(C_SAXIGP5_DATA_WIDTH/8) -1 :0]
      --saxigp5_wlast => s_axi_hp_in(3).W.LAST,
      --saxigp5_wvalid => s_axi_hp_in(3).W.VALID,
      --saxigp5_wready => s_axi_hp_out(3).W.READY,
      --saxigp5_bid => s_axi_hp_out(3).B.ID(5 downto 0), --[5:0]
      --saxigp5_bresp => s_axi_hp_out(3).B.RESP, --[1:0]
      --saxigp5_bvalid => s_axi_hp_out(3).B.VALID,
      --saxigp5_bready => s_axi_hp_in(3).B.READY,
      --saxigp5_arid => s_axi_hp_in(3).AR.ID(5 downto 0), --[5:0]
      --saxigp5_araddr => s_axi_hp_in(3).AR.ADDR, --[48:0]
      --saxigp5_arlen => s_axi_hp_in(3).AR.LEN, --[7:0]
      --saxigp5_arsize => s_axi_hp_in(3).AR.SIZE, --[2:0]
      --saxigp5_arburst => s_axi_hp_in(3).AR.BURST, --[1:0]
      --saxigp5_arlock => s_axi_hp_in(3).AR.LOCK(0),
      --saxigp5_arcache => s_axi_hp_in(3).AR.CACHE, --[3:0]
      --saxigp5_arprot => s_axi_hp_in(3).AR.PROT, --[2:0]
      --saxigp5_arvalid => s_axi_hp_in(3).AR.VALID,
      --saxigp5_aruser => '0', --[15:0]
      --saxigp5_arready => s_axi_hp_out(3).AR.READY,
      --saxigp5_rid => s_axi_hp_out(3).R.ID(5 downto 0), --[5:0]
      --saxigp5_rdata => s_axi_hp_out(3).R.DATA, --[C_SAXIGP5_DATA_WIDTH-1:0]
      --saxigp5_rresp => s_axi_hp_out(3).R.RESP, --[1:0]
      --saxigp5_rlast => s_axi_hp_out(3).R.LAST,
      --saxigp5_rvalid => s_axi_hp_out(3).R.VALID,
      --saxigp5_rready => s_axi_hp_in(3).R.READY,
      --saxigp5_awqos => s_axi_hp_in(3).AW.QOS, --[3:0]
      --saxigp5_arqos => s_axi_hp_in(3).AR.QOS, --[3:0]
      emio_uart1_txd => open,
      emio_uart1_rxd => '0',
      pl_ps_irq0 => STD_LOGIC_VECTOR(TO_UNSIGNED(0, 1)),
      pl_resetn0 => ps_out.FCLKRESET_N,

      pl_clk0 => ps_out.FCLK(0)
    );

  -- Finally we drive the signals out of this module to make the interfaces more AXI4 compliant.
  -- I.e. AXI4 signals that are somehow not driven by the PS hardware
  -- This reduces warnings so we can actually see if anything is not driven that should be
  gm: for i in 0 to C_M_AXI_HP_COUNT-1 generate
    m_axi_hp_out(i).AR.REGION <= (others => '0');
    m_axi_hp_out(i).AR.USER   <= (others => '0');
    m_axi_hp_out(i).AW.REGION <= (others => '0');
    m_axi_hp_out(i).AW.USER   <= (others => '0');
    m_axi_hp_out(i).W.USER    <= (others => '0');
  end generate;
  --gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate
  --  s_axi_hp_out(i).R.USER    <= (others => '0');
  --  s_axi_hp_out(i).B.USER    <= (others => '0');
  --end generate;
end rtl;
