# Guide for developing an OpenCPI Board Support Package (OSP) - Case Study zcu102

## Table Of Contents
1. [Introduction](#introduction)
    1. [Framework Software](#framework-software)
    2. [Embedded SW Platform](#embedded-sw-platform)
    3. [Prerequisites](#prerequisites)
    4. [Preview of the concluding directory tree](#preview-of-the-concluding-directory-tree)
    5. [Review of the vendor's platform documentation and designs](#review-of-the-vendor's-platform-documentation-and-designs)
2. [Design Staging](#design-staging)
    1. [Identify "minimal" configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)
    2. ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf))
3. [OpenCPI Staging](#opencpi-staging)
    1. [Install the OpenCPI framework](#install-the-opencpi-framework)
    2. [Configure a host terminal for OpenCPI development](#configure-a-host-terminal-for-opencpi-development)
    3. [Take inventory of the target board](#take-inventory-of-the-target-board)
    4. [Update the OpenCPI Framework (No action required just a review)](#update-the-opencpi-framework-(no-action-required-just-a-review))
        1. [Enabling Development for New GPP Platforms (Not Applicable)](#enabling-development-for-new-gpp-platforms-(not-applicable))
        2. [Installing the Tool Chain (Not Applicable)](#installing-the-tool-chain-(not-applicable))
        3. [Integrating the Tool Chain into the OpenCPI HDL Build Process (Not Applicable)](#integrating-the-tool-chain-into-the-opencpi-hdl-build-process-(not-applicable))
    5. [Bug fixes to the framework](#bug-fixes-to-the-framework)
    6. [Benchmark testing the zcu104 OSP](#benchmark-testing-the-opencpi-zed-osp)
    7. [Modifications to the Install and Deploy scripts](#modifications-to-the-install-and-deploy-scripts)
    8. [Install and Deploy the platforms: zcu104, xilinx19_2_aarch64](#install-and-deploy-platforms:-zcu104,-xilinx19_2_aarch64)
    9. [Create an OpenCPI project for the OSP](#create-an-opencpi-project-for-the-osp)
4. [Enable OpenCPI HDL Control Plane](#enable-opencpi-hdl-control-plane)
    1. [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)
    2. [PetaLinux workspace for Control Plane](#petalinux-workspace-for-control-plane)
    3. [Configure HDL Primitive for Control Plane](#configure-hdl-primitive-for-control-plane)
    4. [Configure HDL Platform Worker for Control Plane](#configure-hdl-platform-worker-for-control-plane)
    5. [Build the HDL Platform with Control Plane enabled](#build-the-hdl-platform-with-control-plane-enabled)
    6. [HDL Control Plane verification: read OpenCPI "Magic Word"](#hdl-control-plane-verification-read-opencpi-magic-word)
    7. [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled)
    8. [HDL Control Plane verification: test application pattern_capture](#hdl-control-plane-verification:-test-application-pattern_capture)
5. [Enable OpenCPI HDL Data Plane](#enable-opencpi-hdl-data-plane)
    1. [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)
    2. [PetaLinux workspace for Data Plane](#petalinux-workspace-for-data-plane)
    3. [Configure HDL Primitive for Data Plane](#configure-hdl-primitive-for-data-plane)
    4. [Configure HDL Platform Worker for Data Plane](#configure-hdl-platform-worker-for-data-plane)
    5. [Build the HDL Platform with Data Plane enabled](#build-the-hdl-platform-with-data-plane-enabled)
    6. [Undo Edits made to validate HDL Control Plane](#undo-edits-made-to-validate-hdl-control-plane)
    7. [Install and Deploy the HDL Platform with Data Plane enabled](#install-and-deploy-the-hdl-platform-with-data-plane-enabled)
    8. [HDL Data Plane verification: testbias application](#hdl-data-plane-verification:-testbias-application)
    9. [HDL Data Plane verification: test FSK application filerw mode](#hdl-data-plane-verification:-test-fsk-application-filerw-mode)
6. [OpenCPI Component Unit Testing](#opencpi-component-unit-testing)
    1. [Server Mode setup](#server-mode-setup)
    2. [Build and Run](#build-and-run)
7. [Enable OpenCPI Cards & Slots](#enable-opencpi-cards-&-slots)
    1. [Configure HDL Platform Worker with Slot](#configure-hdl-platform-worker-with-slot)
    2. [Building the HDL Platform Worker with Slot enabled](#building-the-hdl-platform-worker-with-slot-enabled)
    3. [Install and Deploy the Slot configuration](#install-and-deploy-the-slot-configuration)
    4. [Cards & Slots verification: FSK modem on the fmcomms2](#cards-&-slots-verification:-fsk-modem-on-the-fmcomms2)
        1. [Building the FSK_modem HDL Assemblies/Containers](#building-the-fsk_modem-hdl-assemblies/Containers)
        2. [Running the FSK_modem_app](#running-the-fsk_modem_app)
    5. [Build and run all Component Unit Tests for Cards & Slots](#build-and-run-all-component-unit-tests-for-cards-&-slots)
8. [APPENDIX](#appendix)
    1. [Petalinux Build](#petalinux-build)
        1. [zcu102 Ethernet Bug fix for PetaLinux](#zcu102-ethernet-bug-fix-for-petalinux)
        2. [Static IP-Address Setup](#static-ip-address-setup)
    2. [Reference Design](#reference-design)
    3. [Network-Mode setup](#network-mode-setup)
    4. [Platform Configuration XML](#platform-configuration-xml)
    5. [Test result tables](#test-result-tables)
        1. [Component Unit Test result table](#component-unit-test-result-table)
        2. [Verification test result table](#verification-test-result-table)
    6. [FMC Daughter Board Connection Table](#fmc-daughter-board-connection-table)  
<br> 

---------------------------------------------------------------------------------------------------
<a name="introduction"></a>

## Introduction
---------------------------------------------------------------------------------------------------

This document serves as a guide for developing an OpenCPI Board Support Package (OSP) for boards based on a Xilinx Zynq UltraScale+ Multi-Processor System-on-a-Chip (MPSoC). Specifically, it is a case study for developing an OSP for the zcu102.  

In addition to the "base" configuration of the platform, two platform configurations are provided in support of the two FMC HPC slots. The “base” configuration supports the command/control, and high throughput data transfer, more commonly referred to as Control Plane and Data Plane, respectively, but does not include device workers. Features that are not included in a “base” configuration are those that are not critical to support the functionality of the Control Plane and Data Plane.  In summary, the "base" configuration OSP is one with the following supported features:

1. Control Plane
2. Data Plane
3. Slot definition(s) for the FMC-HPC

For this OSP, only the FMC-HPC connector slots will be defined to allow support for daughtercards with support for FMC-HPC connectivity.  

The additional platform configurations provided in this OSP define unique configurations and connections of additional device worker combinations that are paired with the platform, in an effort to allow for greater reuse.  

While the zcu102 does have several PMODs, pushbuttons and other peripheral interfaces, NO additional Device Worker support are to be implemented.

Since the Xilinx Zynq UltraScale+ MPSoC on the zcu102 is of the same device family that is on the zcu104, the zcu104's OSP is referenced heavily in this guide. In fact, the zcu104's OSP will be examined in the following areas and will serve as the benchmark for defining the requirements for the zcu102's OSP:

1. What are the output artifacts of performing an OpenCPI Install and Deploy of the zcu104  (and the xilinx19_2_aarch64)
2. Review of the HDL primitive and HDL platform source code for implementing the zcu104
3. Control Plane (CP) ONLY validation - run CP reference application "pattern_capture" on the zcu104 HDL platform
4. Data Plane (DP) validation of the following on the zcu104 HDL platform
    1. DP reference application, i.e. "testbias"
    2. Run FSK "filerw"
    3. Run all Component Unit Tests to establish their Pass/Fail benchmarks
8. Slot (FMC) support - run reference applications fsk_modem_app on the zcu104 with an fmcomms2 daughtercard

<a name="framework-software"></a>

### Framework Software
Because the framework already supports the Xilinx Zynq UltraScale+ processor in support of the zcu104, those portions of the framework's software which perform the command/control and data transport will simply be reused in support of the zcu102 OSPs. As such, this guide will not perform a deep dive into those software sections, but will attempt to make note of the relevant software modules when appropriate.

<a name="embedded-sw-software"></a>

### Embedded SW Platform
In addition to enabling the zcu102's FPGA portion of the SoC, an RCC platform will also be targeted for running artifacts on the SoC's ARM, i.e. the processing system (PS). This requires setup of a cross-compiler for building the run-time utilities of OpenCPI.

By reviewing the table of contents, the developer can gain a high-level understanding of the implementation flow that is performed in this guide. Note that there are various points throughout the guide which require repeating/reusing of steps in previous sections.

**CRITICAL NOTES:**
- **Version v2.1.0 of the OpenCPI FOSS framework is used for this development effort.**
- **Bug fixes to the FOSS are REQUIRED and detailed in the guide.**
- **The zcu104 OSP is used as the reference Zynq UltraScale+ platform for which the zcu102 OCP is based.**
- **For this guide, the default home or working directory is "/home/user".**

**CODE BLOCKS:**  
**Throughout this document there will be references to code that can be found within the repository of the OSP. The directory structure is as follows /home/user/opencpi/projects/osp/ocpi.osp.xilinx/guide/zcu102/\<section>. When necessary, the specific section and file will be outlined.**  
<br>

<a name="deliverables"></a>

### **Deliverables**
---------------------------------------------------------------------------------------------------
1. An OpenCPI project named, "ocpi.osp.xilinx"
2. An OpenCPI Board Support Packages (OSP) with the software platform dependency of "xilinx19_2_aarch64"
    1. zcu102
3. The zcu102 OSP will support the HDL Control Plane, Data Plane, and the FMC-HPC slots
4. Documentation
    1. OSP Getting Started Guide
    2. Guide for developing an OSP for Zynq-UltraScale+ based platforms - Case Study: zcu102 (this guide)
<br>

<a name="prerequisites"></a>

### **Prerequisites**
---------------------------------------------------------------------------------------------------
1. *Knowledge*
    1. A working knowledge of the OpenCPI framework
    2. The developer should be familiar with the various OpenCPI documents, but the following are specifically important for this guide:
        - OpenCPI Platform Development Guide: https://opencpi.gitlab.io/releases/v2.1.0/docs/OpenCPI_Platform_Development_Guide.pdf
        - Component Development Guide (Component Unit Test): https://opencpi.gitlab.io/releases/v2.1.0/docs/OpenCPI_Component_Development_Guide.pdf
        - Getting Started Guide for the zcu104: https://opencpi.gitlab.io/releases/v2.1.0/docs/platform/zcu104_Getting_Started_Guide.pdf
        - Getting Started Guide for the FSK applications: https://opencpi.gitlab.io/releases/v2.1.0/docs/assets/FSK_App_Getting_Started_Guide.pdf
        - Support documentation for the fmcomms2/3 daughtercard

2. *Equipment*

    | Item                 | Description                  | Vendor  | P/N                          | Cost   | 
    |----------------------|------------------------------|---------|------------------------------|--------|
    | zcu102               | Zynq UltraScale+ MPSoC based | Xilinx  | EK-U1-ZCU102-G               | 2495 $ |
    | fmcomms2             | High-speed analog module     | Digikey | AD-FMCOMMS2-EBZ              | 750  $ |

3. *Tools*
    1. Xilinx tools: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/2019-2.html  
        1. Download the "Unified Installer"  
            1. Vivado 2019.2 : build bitstream  
            2. Vitis 2019.2 : cross-compile OCPI runtime utilities for embedded operating system  
        2. PetaLinux 2019: https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools/2019-2.html  
            1. For creating embedded Linux image for SD card  
    2. OpenCPI FOSS framework, v2.1.0: https://gitlab.com/opencpi/opencpi.git  
<br>

<a name="preview-of-the-concluding-directory-tree"></a>

### **Preview of the concluding directory tree**
---------------------------------------------------------------------------------------------------
This guide requires the creation of many directories. A summary of those directories is provided below:

**OpenCPI directory**
~~~
/home/user/opencpi 
~~~

**zcu102 project repo directory**
~~~
/home/user/opencpi/projects/osps/ocpi.osp.xilinx
~~~

**Reference design directory**
~~~
/home/user/vendor_reference_design_package
~~~

**ZynqReleases directory**
~~~
/home/user/2019.2-zcu102-release 
~~~

**Petalinux directories**
~~~
/home/user/plx_zcu102_minimal
/home/user/plx_zcu102_cp
/home/user/plx_zcu102_dp
~~~

**Processing-System vivado project directories**
~~~
/home/user/ps8_zcu102_preset
/home/user/ps8_zcu102_ocpi_cp
/home/user/ps8_zcu102_ocpi_dp
~~~  
<br>

<a name="review-of-the-vendor's-platform-documentation-and-designs"></a>

### **Review of the vendor's platform documentation and designs**
---------------------------------------------------------------------------------------------------
GOALS:
- Allow the user to become familiar with the target board and its reference design package. Review its documentation and work though the reference example designs for both the processor and the FPGA. By working through the reference design, the user will become comfortable with various tools, modifying source code, understanding the build flow (Vivado, PetaLinux), creating an SD card, loading the FPGA and running an application. The [Petalinux Build](#petalinux-build) flow steps captured during this review will be relied upon heavily throughout much of the remainder of this guide and its lesson-learned will be used when integrating with OpenCPI.

It is highly encouraged that the user become familiar with the zcu102 platform. The user can leverage information available the on Xilinx zcu102 webpage below:
https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html
<br>

---------------------------------------------------------------------------------------------------
<a name="design-staging"></a>

## Design Staging
---------------------------------------------------------------------------------------------------

<a name="identify-minimal-configuration-of-ps-core-ip"></a>

### Identify minimal configuration of PS core IP
---------------------------------------------------------------------------------------------------
GOALS:
- Determine the minimal configuration of the PS core IP that will allow the embedded Linux OS to boot and operate normally on the target platform. This step will ensure that the PS core IP is properly configured for communicating with its attached memory.  Once the minimal configuration is verified, then the PS core IP can be modified to enable features for supporting OpenCPI.  
- Two approaches are detailed for ascertaining the minimal configuration of the PS core IP:  
    1. ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) - approach does not include a constraint file.   
    **CRITICAL NOTE: The ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) is the method used and outlined throughout this guide.**
    2. [Reference Design](#reference-design) - has a constraint file available that has been provided by Analog Devices. This approach provides a known "good" constraints file. 
    **CRITICAL NOTE: The [Reference Design](#reference-design) method is located in the APPENDIX for reference and has been confirmed to produce the required artifacts necessary for developing an OSP.**
- Either approach:
    1.  Can be used to generate output products which can be used by the [Petalinux Build](#petalinux-build) flow to build an SD card for successfully booting the embedded OS on the platform
    2. Will result in a Vivado project that provides a starting point for enabling the OpenCPI BSP
    3. Will create a primitive of the Processing System (PS) core IP that is unique to the zcu102. This Processing System (PS) core IP can be wrapped in an OpenCPI primitive and instanced in the OpenCPI platform worker.   
<br>

<a name="preset-board-design-files-(bdf)"></a>

### 'Preset' Board Design Files (BDF)
---------------------------------------------------------------------------------------------------
GOALS:
- Use the Board Design File (BDF) for targeting the zcu102 board, which provides the ability to "Preset" the configuration of the PS core IP that is unique to the target board. The BDF defines things that are unique to the target board, such as the configuration of the DDR.
- This section details creating a new Vivado project based on the Target Board "zcu102" and block design, where a PS core IP (Zynq UltraScale+ MPSoC) is instanced and "Preset", i.e. configured, to support the zcu102.
- The output of this section defines the "minimal" configuration that is Generated, HDL Wrapped and Exported for use in the Petalinux project that is used to create an SD-Card linux image which successfully boots on the target board.
- Once these steps are confirmed, then the signals and ports (clock, reset, master (CP), slave (DP)) that are required for OpenCPI can be included, but this is detailed in a later step.

**NOTE:** All default settings of the PS core IP as a result of applying the 'Preset', are accepted, unless specified in the steps below.

1. Download the appropriate *.bdf file from https://github.com/Xilinx/bdf)
2. Install the *.bdf into /opt/Xilinx/Vivado/2019.2/data/boards/board_files
3. From the /home/user directory, launch Vivado
4. Create a new Vivado project, named "ps8_zcu102_preset"
    - Ensure that the "Create project subdirectory" box is checked, and confirm the "Project Location" is in the desired directory (/home/user/) level prior to clicking "OK". This will provide a separate project for configuring the PS core IP to support the 'preset' only.  
    - **CRITICAL NOTE:  A similar step will be performed when developing support for the CP and the DP. This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
5. Proceed to "Default Part" and select the target the a "Boards" tab, then select "Zynq UltraScale+ ZCU102 Evaluation Board"
6. Open the the Project's Settings window and ensure the Target Language is set to "VHDL"
7. Create Block Design named "preset_config_wrapper"  
**CRITICAL NOTE: Important step, as this naming convention will be used moving forward through this document**
8. Add IP: "Zynq UltraScale+ MPSoC"
9. "Run Block Automation", and accept "Apply Board Preset", and select OK.  
(https://forums.xilinx.com/t5/Processor-System-Design-and-AXI/Zynq7-PS-presets-issue/td-p/1090179)
10. Externalize Ports (ctrl+t)
    1. maxihpm0_fpd_aclk 
    2. maxihpm1_fpd_aclk 
11. On the "Diagram" ribbon select "Validate Design (F6)"
12. In the "Sources" tab and "Hierarchy" view, RMC the "preset_config_wrapper" design and "Create HDL Wrapper"
13. Navigate to "Flow Navigator" -> "IP INTEGRATOR" -> Select "Generate Block Design"
14. After the Generate Block Design is complete, export the hardware by selecting:
    1. File → Export → Export Hardware
    2. Ensure "Include Bitstream" is not checked
    3. Confirmed the "Export to" path
    4. Click Ok
15. The design should look as follows:   
![Preset Design BD](./images/preset_design_BD.png)  
16. Excute the steps detailed in the [Petalinux Build](#petalinux-build) section (example provided below) to update/create the SD card and confirm that the Embedded Linux will boot properly when installed on the zcu102.  
**CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section does not produce a bitstream.**  
<br>
    
    ~~~
    $ cd /home/user

    $ petalinux-create -t project --template zynqMP --name "plx_zcu102_minimal"
    INFO: Create project: plx_zcu102_minimal
    INFO: New project successfully created in /home/user/

    $ cd plx_zcu102_minimal

    $ petalinux-config --get-hw-description=../ps8_zcu102_preset
    INFO: Getting hardware description...
    INFO: Rename preset_config_wrapper_wrapper.xsa to system.xsa
    [INFO] generating Kconfig for project
    [INFO] menuconfig project
    configuration written to /home/user/plx_zcu102_minimal/project-spec/configs/config
     
    *** End of the configuration.
    *** Execute 'make' to start the build or try 'make help'.
     
    [INFO] sourcing bitbake
    [INFO] generating plnxtool conf
    [INFO] generating meta-plnx-generated layer
    [INFO] generating user layers
    [INFO] generating workspace directory
    [INFO] generating machine configuration
    [INFO] generating bbappends for project . This may take time !
    [INFO] generating u-boot configuration files
    [INFO] generating kernel configuration files
    [INFO] generating kconfig for Rootfs
    [INFO] silentconfig rootfs
    [INFO] generating petalinux-user-image.bb
     
    $ petalinux-build
    ........
     
    $ cd images/linux/
     
    $ petalinux-package --boot --fsbl --u-boot --force
    INFO: File in BOOT BIN: "/home/user/plx_zcu102_minimal/images/linux/zynqmp_fsbl.elf"
    INFO: File in BOOT BIN: "/home/user/plx_zcu102_minimal/images/linux/pmufw.elf"
    INFO: File in BOOT BIN: "/home/user/plx_zcu102_minimal/images/linux/bl31.elf"
    INFO: File in BOOT BIN: "/home/user/plx_zcu102_minimal/images/linux/u-boot.elf"
    INFO: Generating zynqmp binary package BOOT.BIN...

     
    ****** Xilinx Bootgen v2019.2
      **** Build date : Oct 23 2019-22:59:42
        ** Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
     
    INFO: Binary is ready.
    WARNING: Unable to access the TFTPBOOT folder /tftpboot!!!
    WARNING: Skip file copy to TFTPBOOT folder!!!
     
    $ cp BOOT.BIN image.ub /run/media/user/sd-card/
    Remove SD card from host
     
    Install SD card into zcu102, and power on zcu102
    From host, start a screen session
     
    $ sudo screen /dev/ttyUSB0 115200
    Zynq>
     
    (!!!THIS NEXT STEP MAY ONLY BE NECESSARY FOR THE MICROZED!!!)
    Zynq> setenv default_bootcmd run uenvboot; run cp_kernel2ram && bootm ${netstart}
    ...
     
    PetaLinux 2019.2 plx_zcu102_minimal /dev/ttyPS0
     
    plx_zcu102_minimal login: root
    Password:
    root@plx_zcu102_minimal:~#
     
    (SUCCESS!!!)
    ~~~
<br>

---------------------------------------------------------------------------------------------------
<a name="opencpi-staging"></a>

## OpenCPI Staging
---------------------------------------------------------------------------------------------------

<a name="install-the-opencpi-framework"></a>

### Install the OpenCPI framework
---------------------------------------------------------------------------------------------------
Download the "v2.1.0" tag of OpenCPI from the provided link, then run the installation for centos7
1. $ cd /home/user
2. $ git clone -b develop https://gitlab.com/opencpi/opencpi.git
3. $ cd opencpi
4. $ git checkout tags/v2.1.0
5. $ ./scripts/install-opencpi.sh  
<br>

<a name="configure-a-host-terminal-for-opencpi-development"></a>

### Configure a host terminal for OpenCPI development
---------------------------------------------------------------------------------------------------
As a convenience, below are the steps for configuring a Host terminal for the OpenCPI development environment
1. After the OpenCPI framework has been installed, source the OpenCPI framework setup script  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s
2. Ensure the environment is configured for the desire version of Vivado and its license file  
    $ export OCPI_XILINX_LICENSE_FILE=2100@r420  
    $ export OCPI_XILINX_VIVADO_VERSION=2019.2  
    $ env | grep OCPI
    ~~~
    $ env | grep OCPI
    OCPI_CDK_DIR=/home/user/opencpi/cdk
    OCPI_PREREQUISITES_DIR=/home/user/opencpi/prerequisites
    OCPI_ROOT_DIR=/home/user/opencpi
    OCPI_TOOL_ARCH=x86_64
    OCPI_TOOL_DIR=centos7
    OCPI_TOOL_PLATFORM=centos7
    OCPI_TOOL_PLATFORM_DIR=/home/user/opencpi/cdk/../project-registry/ocpi.core/exports/rcc/platforms/centos7
    OCPI_TOOL_OS_VERSION=c7
    OCPI_TOOL_OS=linux
    OCPI_XILINX_LICENSE_FILE=2100@r420
    OCPI_XILINX_VIVADO_VERSION=2019.2
    ~~~  
<br>

<a name="take-inventory-of-the-target-board"></a>

### Take inventory of the target board
---------------------------------------------------------------------------------------------------
BACKGROUND:  
Per the OpenCPI Platform Development Guide, the target board will be examined to identify the devices and interfaces that are required to support a functioning OpenCPI BSP. One of the main focuses of this phase is to identify the device(s) that will be implemented to support an OpenCPI “container”, which is where an application or portion of an application can be deployed. The interfaces between all devices are examined to determine if they are necessary for command/control or to transport payload data within an OpenCPI BSP. It is necessary to establish which device or interconnect to an external device serves as the “main processor”, and whose responsibility it is to perform system level functionality like, command/control, and possibly, transportation of payload data. This will aid in determining the amount of work that is necessary for developing an OpenCPI BSP to the target platform.

GOALS:
1. Identify container(s)
2. Identify interface(s)
3. Sort these items into the following categories based on their current support within the OpenCPI framework: \*\*Yes, No, Partial.\*\* For items where there is partial or no support, a research phase is conducted to ascertain the amount of development that is required to understand the level of support that is needed by the target platform. The “level of support” varies based on the configuration for a given device versus the requirements of the target platform. For example, although the digital interface of the Analog Devices transceiver may support LVDS and CMOS electrical standards, the target platform may implement support of only the LVDS standard, and therefore it may not be necessary to also enable support for CMOS. However, implementing support for CMOS standard could be added at a later time. This also highlights any additional requirement(s) for developing support for new tooling or other features that must be integrated into the OpenCPI framework to properly target the devices, such as FPGA build tools or software cross-compilers. In many cases, this initial investigation into the platform can be accomplished from vendor provided documentation, such as,  the User’s guide, board schematics and wiring diagrams. In cases where this documentation is insufficient, analysis must be performed with access to the board directly. However, it is also possible that not enough information can be gathered to successfully develop an OpenCPI BSP, but this should be determined upon the completion of the phases described above.

    |Tool/Device/Interface/Function | Description            | Framework Support (Yes/No/Partial)    |
    |-------------------------------|------------------------|---------------------------------------|
    | T: FPGA build tools           | Xilinx Vivado 2019.2   | Yes                                   |
    | D: xczu9eg-ffvb1152-2-e       | Zynq UltraScale+ MPSoC | Yes - but not tested on this platform |
    | I: FMC-HPC                    | FMC High Pin Count     | Yes                                   |  
<br>

<a name="update-the-opencpi-framework-(no-action-required-just-a-review)"></a>

### Update the OpenCPI Framework (No action required just a review)
---------------------------------------------------------------------------------------------------
Based on the items identified in the [Take inventory of the target board](#take-inventory-of-the-target-board) section, the below is a high-level summary of updates made to the OpenCPI framework, necessary in support of this guide. 

**NOTE:** Some updates are "Not Applicable", but listed to emphasize that they were considered. 
<br> 

<a name="enabling-development-for-new-gpp-platforms-(not-applicable)"></a>

#### Enabling Development for New GPP Platforms (Not Applicable)  
- The SW platform version (xilinx19_2_aarch64) is supported by the framework. This was determined by reviewing the supported RCC platforms located in /home/user/opencpi/projects/core/rcc/platforms/.
<br>  

<a name="installing-the-tool-chain-(not-applicable)"></a>

#### Installing the Tool Chain (Not Applicable)
- The FPGA build tools/version (Xilinx Vivado/Vitis 2019.2) is supported by the framework.
<br>

<a name="integrating-the-tool-chain-into-the-opencpi-hdl-build-process-(not-applicable)"></a>

#### Integrating the Tool Chain into the OpenCPI HDL Build Process (Not Applicable)
- The target family and device is supported by the framework. This was determined by reviewing the contents of /home/user/opencpi/tools/include/hdl/hdl-targets.mk.  
<br>

<a name="bug-fixes-to-the-framework"></a>

### Bug fixes to the framework
---------------------------------------------------------------------------------------------------
**CRITICAL NOTES:**  
- **The following bug fix MUST be applied to the v2.1.0 framework release to address an intermittent run-time failure observed on the embedded system while running in Network and Server Modes.**  
- **The implementation of this bug fix is required PRIOR to installing (i.e. cross-compiling) the OpenCPI run-time utilities for any targeted embedded SW RCC Platform, such as, the xilinx19_2_aarch64.**

~~~
$ git diff runtime/dataplane/transport/src/OcpiInputBuffer.cxx
WARNING: terminal is not fully functional
-  (press RETURN) 
diff --git a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
index 3d37c6e..03276cd 100644
--- a/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
+++ b/runtime/dataplane/transport/src/OcpiInputBuffer.cxx
@@ -147,7 +147,7 @@ void InputBuffer::update(bool critical)
        sizeof(BufferMetaData)*MAX_PCONTRIBS);
 
     memset(m_bmdVaddr,0,sizeof(BufferMetaData)*MAX_PCONTRIBS);
-    getPort()->getEndPoint().doneWithInput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
+    getPort()->getEndPoint().doneWithOutput(m_bmdVaddr, sizeof(BufferMetaData)*MAX_PCONTRIBS);
     m_sbMd = static_cast<volatile BufferMetaData (*)[MAX_PCONTRIBS]>(m_bmdVaddr);
   }
~~~
<br>

<a name="benchmark-testing-of-opencpi-zed-osp"></a>

### Benchmark testing the OpenCPI zcu104 OSP
---------------------------------------------------------------------------------------------------
GOALS:
- Gain a benchmark understanding of build-time and run-time utilities, as they are performed for the OpenCPI zcu104 HDL platform and its paired SW RCC platform, xilinx19_2_aarch64.
    - Build "known good" RCC and HDL platforms to aid in the development of the OSP for the zcu102
    - Understand the impact of the contents of the zcu104.exports file by reviewing the outputs of install/deploy of the zcu104
    - Build the "canary" Control Plane (CP)-ONLY HDL bitstream and run its application
    - Build the "canary" Data Plane (DP) HDL bitstreams and run applications
    - Build the Component Unit Tests and run them on the zcu104 to establish a benchmark performance metrics. These benchmark performance metrics for the zcu104 are outlined in the [Component Unit Test result table](#component-unit-test-result-table) section.
<br>

<a name="modifications-to-the-install-and-deploy-scripts"></a>

### Modifications to the Install and Deploy scripts
---------------------------------------------------------------------------------------------------
GOALS:
- By default, the "testbias" HDL assembly (Control + Data Plane) is built as part of the installation process for a given OSP, and this bitstream is included in the deployment of said OSP. The purpose of these modifications are to replace the "testbias" HDL assembly, so that, the "canary" Control Plane-ONLY HDL assembly "pattern_capture" is built (i.e. bitstream is created), the OAS XML file (pattern_capture.xml) are properly deployed and available for running on the zcu104.

1. For the "canary" Control Plane-ONLY app (pattern_capture.xml) to be included in the list of OASs that are exported to /home/user/opencpi/cdk/\<rcc-platform>/sd*/opencpi/applications,  create a symbolic link within the /home/user/opencpi/projects/assets/applications/ to pattern_capture.xml  
    - $ cd /home/user/opencpi/projects/assets/applications/  
    - $ ln -s pattern_capture/pattern_capture.xml pattern_capture.xml
2. Edit the following scripts to target the assembly "pattern_capture_asm", rather than the "testbias" assemby:  
    **CRITICAL NOTES:**  
    - **It is recommended to perform a 'Find and Replace' for all occurances of "testbias" with "pattern_capture" in the files listed below. A syntax error in these files can be difficult to diagnose, therefore it is NOT recommended to simply comment out and replace the lines when making these edits.**  
    - **In a later section [Undo Edits made to validate HDL Control Plane](#undo-edits-made-to-validate-hdl-control-plane), these edits will be reverted back to their original state, so that, the "testbias" will be installed/deployed in support of enabling the Data Plane.**
   1. Edit the `export-platform-to-framework.sh` script to target pattern_capture_asm 
        - Edit /home/user/opencpi/tools/scripts/export-platform-to-framework.sh  
            - FROM: tbz=projects/assets/exports/artifacts/ocpi.assets.testbias_${platform}_base.hdl.0.${platform}.bitz
            - TO: tbz=projects/assets/exports/artifacts/ocpi.assets.pattern_capture_asm_${platform}_base.hdl.0.${platform}.bitz  
   1. Edit the `ocpiadmin.sh` script to target pattern_capture_asm 
        - Edit /home/user/opencpi/tools/scripts/ocpiadmin.sh  
            1. Edit #1
            - FROM: ocpidev -d projects/assets build --hdl-platform=$platform hdl assembly testbias
            - TO: ocpidev -d projects/assets build --hdl-platform=$platform hdl assembly pattern_capture_asm  
            2. Edit #2
            - FROM: echo "HDL platform \"$platform\" built, with one HDL assembly (testbias) built for testing."
            - TO: echo "HDL platform \"$platform\" built, with one HDL assembly (pattern_capture_asm) built for testing." 
    3. Edit the `deploy-platform.sh` script to target pattern_capture_asm  
    **CRITICAL NOTE:**
        - **Be mindfull of the source-code spacing, the spacing below is not correct due to Markdown limitations, so do not copy and paste from here but copy and paste your source code and replace 'testbias' with 'pattern_capture_asm' in the appropriate location.**
        - Edit /home/user/opencpi/tools/scripts/deploy-platform.sh  
            - FROM: cp $verbose -L ../projects/assets/hdl/assemblies/testbias/container-testbias_${hdl_platform}_base/target-*/*.bitz \  
            $sd/opencpi/artifacts
            - TO: cp $verbose -L ../projects/assets/hdl/assemblies/pattern_capture_asm/container-pattern_capture_asm_${hdl_platform}_base/target-*/*.bitz \  
            $sd/opencpi/artifacts
<br>

<a name="install-and-deploy-platforms:-zcu104,-xilinx19_2_aarch64"></a>

### Install and Deploy platforms: zcu104, xilinx19_2_aarch64
---------------------------------------------------------------------------------------------------
GOAL:
- "Known good" HDL and RCC platforms are installed and deployed to aid in the development of the OpenCPI BSP for the zcu102. They are used to validate the installation of the framework and to ensure the run-time utilities performed on the zcu104 as expected, prior to enabling the zcu102.
- **CRITICAL NOTE:**
    - **The zcu102 HDL platform will not be installed here as it has not yet been built.**
1. Setup the Software cross-compiler 
    - The following commands are outlined in the OpenCPI Installation Guide here:  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf (PG. 57)   
    1. ZynqReleases implementation   
        - Download the prebuilt Linux image for the zcu104 board platform  
        1. Go to the Xilinx Wiki page at wiki.xilinx.com  
        2. Select the Linux Prebuild Images page  
        3. Select 2019.2 Release  
        4. Navigate/scroll to the Downloads section  
        5. Download the 2019.2-zcu104-release.tar.xz
        6. $ sudo mkdir -p /opt/Xilinx/ZynqReleases/2019.2/
        7. $ cd /home/user/Downloads
        8. $ sudo cp 2019.2-zcu104-release.tar.xz /opt/Xilinx/ZynqReleases/2019.2
        9. $ sudo chown -R \<user>:users /opt/Xilinx/ZynqReleases
            - Example: sudo chown -R smith:users /opt/Xilinx/ZynqReleases
            - **NOTE:** This may require adjusting the permissions for /opt/Xilinx/ZynqReleases or its subdirectories.   
    2. git implementation   
        1. $ sudo mkdir -p /opt/Xilinx/git
        2. $ cd /opt/Xilinx/git
        3. $ sudo git clone --branch xilinx-v2019.2.01 https://github.com/Xilinx/linux-xlnx.git
            - (TYPO: in OCPI doc xilinx_v2019.2)
        4. $ sudo git clone --branch xilinx-v2019.2 https://github.com/Xilinx/u-boot-xlnx.git
            - (TYPO: in OCPI doc xilinx_v2019.2)
        5. $ sudo chown -R \<user>:users /opt/Xilinx/git
            - Example: sudo chown -R smith:users /opt/Xilinx/git   
            - **NOTE:** This may require adjusting the permissions for /opt/Xilinx/git or its subdirectories.

2. CD into the OpenCPI directory  
$ cd /home/user/opencpi
3. Setup terminal for OpenCPI development  
$ source ./cdk/opencpi-setup.sh -s  
4. Install: xilinx19_2_aarch64 (an RCC platform)  
$ ocpiadmin install platform xilinx19_2_aarch64
5. Install: zcu104 (an HDL platform) **NOTE:** Estimated time ~14+ Hours  
$ ocpiadmin install platform zcu104
6. Deploy: zcu104 with xilinx19_2_aarch64  
$ ocpiadmin deploy platform xilinx19_2_aarch64 zcu104  
<br>

<a name="create-an-opencpi-project-for-the-osp"></a>

### Create an OpenCPI project for the OSP
---------------------------------------------------------------------------------------------------
GOAL:
- To create a skeleton project directory for the zcu102 OSP and update the project-registry to include the said project.

1. Create a project, under /home/user/opencpi/projects/osps  
    $ cd /home/user/opencpi/projects/osps/  
    $ ocpidev create project ocpi.osp.xilinx      
    $ cd ocpi.osp.xilinx
2. Edit `Project.mk`   
    ~~~
    PackageName=osp.xilinx
    PackagePrefix=ocpi
    ProjectDependencies=ocpi.platform ocpi.assets
    ComponentLibraries+=misc_comps util_comps dsp_comps comms_comps
    ~~~
3. Register ocpi.osp.xilinx project  
    $ ocpidev register project 
    ~~~
    $ ocpidev register project
    Successfully registered the ocpi.osp.xilinx project: /home/user/opencpi/projects/osps/ocpi.osp.xilinx
    Into the registry: /home/user/opencpi/project-registry
    
    make: Entering directory `/home/user/opencpi/projects/osps/ocpi.osp.xilinx'
    make: Leaving directory `/home/user/opencpi/projects/osps/ocpi.osp.xilinx'
    ~~~
4. Confirm that the ocpi.osp.xilinx project is registered  
    $ ocpidev show registry
    ~~~
    Project registry is located at: /home/user/opencpi/project-registry
    --------------------------------------------------------------------------------------------------
    | Project Package-ID  | Path to Project                                            | Valid/Exists |
    | ------------------  | -----------------------------------------------------------| ------------ |
    | ocpi.core           | /home/user/opencpi/projects/core                           | True         |
    | ocpi.tutorial       | /home/user/opencpi/projects/tutorial                       | True         |
    | ocpi.assets_ts      | /home/user/opencpi/projects/assets_ts                      | True         |
    | ocpi.assets         | /home/user/opencpi/projects/assets                         | True         |
    | ocpi.platform       | /home/user/opencpi/projects/platform                       | True         |
    | ocpi.osp.xilinx     | /home/user/opencpi/projects/osps/ocpi.osp.xilinx           | True         |
    --------------------------------------------------------------------------------------------------
    ~~~
<br>

<a name="enable-opencpi-hdl-control-plane"></a>

## Enable OpenCPI HDL Control Plane
---------------------------------------------------------------------------------------------------
<a name="configure-ps-for-opencpi-hdl-control-plane"></a>

### Configure PS for OpenCPI HDL Control Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Configure the PS core IP to enable and expose signals/ports, per the requirements of the OpenCPI HDL Control Plane control software for the Zynq UltraScale+ MPSoC devices:
    - Clock(s)
    - Reset(s)
    - An AXI Master interface
        - Memory mapping much match with that defined in HdlZynq.h

**NOTE: As a result of applying the 'Preset', all default settings of the PS core IP are accepted unless specified in the steps below**.

1. These steps continue with the completion of the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section
2. Open the Vivado "ps8_zcu102_preset" project, File -> Project -> Save As ->  "ps8_zcu102_ocpi_cp"  
- Ensure that the "Create project subdirectory" box is checked, and confirm the "Project Location" is in the desired directory (/home/user/) level prior to clicking "OK". This will provide a separate project for configuring the PS core IP to support the CP only OSP. 
- **CRITICAL NOTE:  A similar step will be performed when developing support for the DP. This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
3. Open the Block Design -> double click the zynq_ultra_ps_e_0
4. Navigate to "PS-PL Configuration" -> "PS-PL Interfaces" -> "Master Interface" -> Disable (Uncheck) "AXI HPM1 FDP" 
5. From the same location, select the "AXI HPM0 FDP" port, select "32" for the "AXI HPM0 FPD Data Width" -> "OK"
6. Remove the old "maxihpm1_fpd_aclk_0" externalized port, RMC "maxihpm1_fpd_aclk_0" port -> Select "Delete"
7. Remove the "maxihpm0_fpd_aclk0" externalized port, RMC "maxihpm0_fpd_aclk_0" port -> Select "Delete"
8. Externalize the "pl_clk0" port, LMC "pl_clk0" then Ctrl+t
9. Create a connection between maxihpm0_fpd_aclk and pl_clk0
7. Return to "Diagram" and "Validate Design" (F6)
8. Flow Navigator window → IP INTEGRATOR → Generate Block Design -> Generate   
    **CRITCAL NOTE: Recall from the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section, that "Create HDL Wrapper" was performed, because it is a prerequiste for performing "Export Hardware".** 
    1. Performing "Generate Block Design", should automatically update the HDL Wrapper file, if the defaults setting was accepted to allow Vivado to auto-manage updating the wrapper.
9. After the Generate Block Design is complete, export the hardware by selecting
    1. File → Export → Export Hardware
    2. DO NOT check the box for "Include Bitstream"
    2. Confirm the "Export to" path is correct
    3. Select "OK"
10. The design should look as follows:  
![Control Plane BD](./images/control_plane_BD.png)  
<br>

<a name="petalinux-workspace-for-control-plane"></a>

### PetaLinux workspace for Control Plane
---------------------------------------------------------------------------------------------------
1. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly.  
2. Create the PetaLinux workspace for the development of Control Plane ONLY SD card image   
    $ /home/user/plx_zcu102_cp  
    **CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section has not produce a bitstream.**
<br>

<a name="configure-hdl-primitive-for-control-plane"></a>

### Configure HDL Primitive for Control Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Create an OpenCPI HDL primitive that wraps the Zynq UltraScale+ MPSoC PS core IP which has been configured per the settings of the zcu102. Since the zcu104 OpenCPI HDL Platform targets the same device family, its HDL primitive module is used as a reference implementation for this task.

**CODE BLOCK:**   
**The code block for the various files that make up the HDL Primitive can be found in the following directory of the ocpi.osp.xilinx repository:**  
- **ocpi.osp.xilinx/guide/zcu102/Enable_OpenCPI_Control-Plane/Primitive/**

1. Setup terminal for OpenCPI development   
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s
2. Create an OpenCPI HDL primitive library, named "zynq_ultra_zcu102"  
    $ cd projects/osps/ocpi.osp.xilinx  
    $ ocpidev create hdl primitive library zynq_ultra_zcu102
3. From the Vivado project modified in [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane) and which is specific to using the "Preset" to configure the PS core IP for the zcu102 , browse to the generated artifacts directory, and copy them into the newly created OpenCPI HDL primitive library.
    1. $ cd opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102
    2. $ cp -rf /home/user/ps8_zcu102_ocpi_cp/ps8_zcu102_ocpi_cp.srcs/sources_1/bd/preset_config_wrapper/ip/preset_config_wrapper_zynq_ultra_ps_e_0_0/ /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102
4. Since the zcu102 is very similar to the zcu104, we can simply copy and rename a couple files from the platform/hdl/primitive/zynq HDL primitive library into the zynq_ultra_zcu102 and edit as needed.
    1. $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102
    2. $ cp /home/user/opencpi/projects/platform/hdl/primitives/zynq_ultra/zynq_ultra_pkg.vhd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102/zynq_ultra_zcu102_pkg.vhd
    3. $ cp /home/user/opencpi/projects/platform/hdl/primitives/zynq_ultra/zynq_ultra_ps.cpp_vhd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102/zynq_ultra_zcu102_ps.vhd  
    - **CRITICAL NOTE:**
    **This guide does not perform the C++ preprocessing on the zynq_ps.cpp_vhd, that is described in the platform/hdl/primitives/zynq/Makefile. All C++ preprocessing will be removed in subsequent steps.**
5. Edit the "zynq_ultra_zcu102_ps.vhd", to remove all C++ preprocessing code and to normalize the interface of the generated PS7 core IP to OpenCPI Control Plane signaling.
    1. Change library names from: 
        - "library zynq_ultra;" to "library zynq_ultra_zcu102;"
        - "use zynq_ultra.zynq_ultra_pkg.all;" -> "use zynq_ultra_zcu102.zynq_ultra_zcu102_pkg.all; "
    2. Comment out the s_axi_hp ports from the entity (these are for the Data Plane, and will be added back later) 
    3. Change entity name from "zynq_ultra_ps" to "zynq_ultra_zcu102_ps"
    4. Replace the PS8_WRAPPER_MODULE that is in the architecture declaration and body with the the PS7 core IP primitive from the "Preset" project
        1. Start by opening the preset_config_wrapper_zynq_ultra_ps_e_0_0/preset_config_wrapper_zynq_ultra_ps_e_0_0_stub.vhdl to observe those signals which need to remain in the "zynq_ultra_zcu102_ps.vhd"
        2. i.e Change component and instance from "PS8_WRAPPER_MODULE" to "preset_config_wrapper_zynq_ultra_ps_e_0_0", remove all generics and signals that are not shown in the preset_config_wrapper_zynq_ultra_ps_e_0_0_stub.vhdl  
    5. Change architecture name from "zynq_ultra_ps" to "zynq_ultra_zcu102_ps"
    6. Once step 5 is complete all that should remain are the following ports:
        1. maxihpm0 (Control Plane Clock)
        2. maxigp0 (Control Plane Ports)
        3. saxihp0_fpd_aclk, saxihp1_fpd_aclk, saxihp2_fpd_aclk, saxihp3_fpd_aclk (Data Plane Clocks)
        4. saxigp2, saxigp3, saxigp4, saxigp5 (Data Plane Ports)
        6. pl_clk0
        8. pl_resetn0
        7. gm: for i in - to C_M_AXI_HP_COUNT-1 code block
        8. gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate code block
    7. Of the remaining ports left, comment out the following ports (these are for the Data Plane, and will be added back later)
        1. saxihp0_fpd_aclk, saxihp1_fpd_aclk, saxihp2_fpd_aclk, saxihp3_fpd_aclk (Data Plane Clocks)
        2. saxigp2, saxigp3, saxigp4, saxigp5  (Data Plane Ports)
6. Edit the HDL package "zynq_ultra_zcu102_pkg.vhd"  
    1. Change package name from "zynq_ultra_pkg" to "zynq_ultra_zcu102_pkg"
    2. Change primitive component name from "zynq_ultra_ps" to "zynq_ultra_zcu102_ps"
    3. Edit the C_M_AXI_HP_COUNT constant from 2 to 1  
        FROM:  
        ~~~
        constant C_M_AXI_HP_COUNT : natural := 2;
        ~~~
        TO:
        ~~~
        constant C_M_AXI_HP_COUNT : natural := 1;
        ~~~ 
    3. Comment out the s_axi_hp_in and s_axi_hp_out ports (These are for the Data Plane, and will be added back later)  
8. Update the primitive library's Makefile to specify all of the dependencies:  
    /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102/Makefile  
9. Return to the top of the project and build the primitive library:  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
    $ ocpidev build --hdl-target zynq_ultra
    ~~~
    $ ocpidev build --hdl-target zynq_ultra
    No HDL platforms specified.  No HDL assets will be targeted.
    Possible HdlPlatforms are: alst4 alst4x isim matchstiq_z1 ml605 modelsim x4sim xsim zcu104 zcu104 zcu104_ise.
    make[1]: Entering directory `/home/user/opencpi/projects/osps/ocpi.osp.xilinx'
    make[1]: Leaving directory `/home/user/opencpi/opencpi_v2.1.0/projects/osps/ocpi.osp.xilinx'
    ============== For library zynq_ultra_zcu102:
    Building the zynq_ultra_zcu102 library for zynq_ultra (target-zynq_ultra/zynq_ultra_zcu102) 0:()
     Tool "vivado" for target "zynq_ultra" succeeded.  0:00.02 at 14:13:29
    Creating directory ../lib/zynq_ultra_zcu102 for library zynq_ultra_zcu102
    No previous installation for gen/zynq_ultra_zcu102.libs in ../lib/zynq_ultra_zcu102.
    Installing gen/zynq_ultra_zcu102.libs into ../lib/zynq_ultra_zcu102
    No previous installation for target-zynq_ultra/zynq_ultra_zcu102.sources in target-zynq_ultra/zynq_ultra_zcu102.
    Installing target-zynq_ultra/zynq_ultra_zcu102.sources into target-zynq_ultra/zynq_ultra_zcu102
    No previous installation for target-zynq_ultra/zynq_ultra_zcu102 in ../lib/zynq_ultra_zcu102/zynq_ultra.
    Installing target-zynq_ultra/zynq_ultra_zcu102 into ../lib/zynq_ultra_zcu102/zynq_ultra
    ~~~
<br>

<a name="configure-hdl-platform-worker-for-control-plane"></a>

### Configure HDL Platform Worker for Control Plane

---------------------------------------------------------------------------------------------------
**CODE BLOCK: The code block for the various files that make up the HDL platform worker can be found in the following directory of the ocpi.osp.xilinx repository:**  
- **/home/user/opencpi/projects/osps/ocpi.osp.xilinx/guide/zcu102/Enable_OpenCPI_Control-Plane/Platform-Worker/**

1. Create HDL Platform Worker:  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
    $ ocpidev create hdl platform zcu102    
2. Change directory to HDL Platform Worker  
    $ cd hdl/platforms/zcu102
3. Copy zcu104.xml into the zcu102 directory and rename it zcu102.xml, and edit
    1. $ cp /home/user/opencpi/projects/platform/hdl/platforms/zcu104/zcu104.xml /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/zcu102.xml
    2. Chage the name of the paltform from "zcu104" to "zcu102"  
        ```<SpecProperty Name='platform' Value='zcu102'/>```
    3. Comment out SpecProperties: nSlots, nSwitches, slotNames: only the following SpecProperties shall remain:  
        ```<SpecProperty name='platform' value='zcu102'/>```  
        ```<specproperty name="nLEDs" parameter='1' value='4'/>```
    4. Change the nLEDs value to 8  
        ```<specproperty name="nLEDs" parameter='1' value='8'/>```
    4. Comment out the sdp module
    5. Comment out Properties: axi_error, and sdpDropCount, all debug properties.  
    6. Remove the following useGP1 Property Name
        ```<Property name='useGP1' type='bool' parameter='1' default='false'/>```
    6. Leave the following led signals in order to attach an LED counter
        ~~~
        <signal  output='leds' width='8'/>
        <signal name='leds(0)' platform="GPIO_LED_0"/>
        <signal name='leds(1)' platform="GPIO_LED_1"/>
        <signal name='leds(2)' platform="GPIO_LED_2"/>
        <signal name='leds(3)' platform="GPIO_LED_3"/>
        ~~~
    7. Add the following led signals in order to have 8 GPIO_LED signals
        ~~~
        <signal name='leds(4)' platform="GPIO_LED_4"/>
        <signal name='leds(5)' platform="GPIO_LED_5"/>
        <signal name='leds(6)' platform="GPIO_LED_6"/>
        <signal name='leds(7)' platform="GPIO_LED_7"/>
        ~~~
    6. Comment out signals: fmc_prsnt
    7. Comment out the slot declaration
4. Copy zcu104.vhd into the zcu102 directory and rename it zcu102.vhd, and edit
    1. $ cp /home/user/opencpi/projects/platform/hdl/platforms/zcu104/zcu104.vhd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/zcu102.vhd
    2. Library "zynq_ultra" → "zynq_ultra_zcu102"
    3. "zynq_ultra.zynq_ultra_pkg.all" -> "zynq_ultra_zcu102.zynq_ultra_zcu102_pkg.all"
    4. Change 'ps : zynq_ultra_zcu104_ps' to 'ps : zynq_ultra_zcu102_ps' 
    5. Remove the following whichGP/useGP1 signals and notes:
        1. Remove 1
            ~~~
            -- Note: useGP1/whichGP are used in Zynq platforms but require PS/SW to detect this property's value.
            --       This is not yet supported on ZynqMP/UltraScale+, so it is tied to '0' to use GP0.
            --constant whichGP : natural := to_integer(unsigned(from_bool(useGP1)));
            constant whichGP : natural := to_integer(unsigned(from_bool('0')));
            ~~~
        2. Remove 2
            ~~~
            --   Note: useGP1 is used on Zynq platforms but requires the PS/SW to detect this property's value.
            --         This is not yet suppported on ZynqMP/UltraScale, so it is tied to '0' to use GP0.
            --     ps_in.debug           => (31 => useGP1, others => '0'),
            ~~~
    6. Edit the following whichGP/useGP1 signals and notes
        1. Edit 1  
            FROM:
            ~~~
            ps_in.debug           => (31 => useGP1, others => '0'),
            ~~~
            TO:
            ~~~
            ps_in.debug           => (others => '0'),
            ~~~
        2. Edit 2  
            FROM:  
            ~~~
            axi_in  => ps_m_axi_gp_out(whichGP),
            axi_out => ps_m_axi_gp_in(whichGP),
            ~~~
            TO:
            ~~~
            axi_in  => ps_m_axi_gp_out(0),
            axi_out => ps_m_axi_gp_in(0),
            ~~~
    7. Comment out the global clock buffer and make the new clk <= fclk(0) assignment:
        ~~~
        -- Use a global clock buffer for this clock used for both control and data
        -- clkbuf : BUFG port map(I => fclk(0),
        --                       O => clk);
        -- PS8 primitive already applies a BUFG to the fclks
        clk <= fclk(0);
        ~~~
    8. Comment out the following signals:  
        1. all Data Plane related circuitry: any signal with "sdp" or "hp"  
        2. "rst_n"  
        3. "dbg" signals  
        4. "zynq_ultra_out and zynq_ultra_out_data  
        5. "g : for i in 0 to C_M_AXI_HP_COUNT-1 generate..." code block  
        6. props_out.switches  <= resize(unsigned(switches), props_out.switches'length);
        7. leds <= std_logic_vector(props_in.led(leds'range));
        9. the final clocked process
    9. Underneath the commented out 'leds' signal Create the following 'leds' signal
        ~~~
        leds(7 downto 4) <= std_logic_vector(props_in.LEDs(7 downto 4));
        ~~~
    10. After the first clocked process include the following LEDs:
        ~~~
        leds(0) <= count(count'left);
        leds(1) <= count(count'left-1);
        leds(2) <= reset;
        leds(3) <= '1';
        ~~~ 
    11. After the LEDs create a counter for the LEDs
        ~~~
        work : process(clk)
        begin
          if rising_edge(clk) then
            if reset = '1' then
              count <= (others => '0');
            else
              count <= count + 1;
            end if;
          end if;
        end process;
        ~~~
5. Create zcu102.xdc
    1. The constraints file can be found here:  
    https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html#documentation
    2. Use the "Document Type" filter to find "Board Files"
    3. Under "XTP454-ZCU102 Schematics (v1.0)" Select "zcu102-xdc-rdf0405.zip"
    4. Unzip "zcu102-xdc-rdf0405.zip" and place the "zcu102_Rev1.0_U1_09152016.xdc" file into the the zcu102 Platform Worker.  
        $ cp /home/user/Downloads/zcu102_Rev1.0_U1_09152016.xdc /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102  
    5. Once the original is copied into the zcu102 Platform Worker. Make a copy to edit and rename it zcu102.xdc  
        $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102  
        $ cp zcu102_Rev1.0_U1_09152016.xdc zcu102.xdc  
    7. Comment out All ports except the following: "GPIO_LED_0" - "GPIO_LED_7", i.e. leds 0-7.
    8. At the bottom of the zcu102.xdc constraints file add the following line:
        - **CRITICAL NOTE: Signal paths can be different if the HDL is based on VHDL vs Verilog**
        ~~~
        # OpenCPI additions to the above, which is unmodified from the original
        create_clock -name clk_fpga_0 -period 10.000 [get_pins -hier * -filter {NAME =~ */ps/U0/U0/PS8_i/PLCLK[0]}]
        set_property DONT_TOUCH true [get_cells "ftop/pfconfig_i/zcu102_i/worker/ps/ps/U0/PS8_i"]
        ~~~
6. Copy the zcu104/Makefile into /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/Makefile and edit such that its contents match the provided CODE BLOCK
7. Copy/rename the zcu104/zcu104.mk into /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/zcu102.mk and edit such that its contents match the provided CODE BLOCK
8. Copy/rename the zcu104/zcu104.exports into /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/zcu102.exports and edit such that its contents match the provided CODE BLOCK
9. Copy/rename the zcu104/98-zcu104.rules into /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/98-zcu102.rules and edit such that its contents match the provided CODE BLOCK
10. Create a zcu102_bit.xdc, which is to remain empty. By remaining empty, this signifiying that all defaults are used by Vivado.
11. Create an "sd_card" directory
12. Copy /home/user/opencpi/platforms/zynq/zynq_system.xml into the sd_card/ and rename "system.xml" and edit the system.xml to look like the following:
    ~~~
    <opencpi>
        <container>
            <rcc load='1'/>
            <remote load='1'/>
            <hdl load='1'>
                <device name='PL:0' platform='zcu102'/>
            </hdl>
        </container>
        <transfer smbsize='128K'>
            <pio load='1' smbsize='10M'/>
            <dma load='1'/>
            <socket load='1'/>
        </transfer>
    </opencpi>
    ~~~  
<br>

<a name="build-the-hdl-platform-with-control-plane-enabled"></a>

### Build the HDL Platform with Control Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Build the HDL Platform Worker and "base" Platform Configuration
- Verify that the HDL platform is recognized by the framework

1. Build the HDL platform zcu102  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
    $ ocpidev build --hdl-platform zcu102  
2. Confirm that the zcu102 is recognized by the framework as a valid HDL platform target:  
    $ ocpidev show platforms  
    ~~~
    -------------------------------------------------------------------------------------------------------------------------------------------
    | Platform           | Type | Package-ID                                  | Target              | HDL Part                   | HDL Vendor |
    | ------------------ | ---- | --------------------------------------------| --------------------| -------------------------- | ---------- |
    | centos7            | rcc  | ocpi.core.platforms.centos7                 | linux-c7-x86_64     | N/A                        | N/A        |
    | xilinx19_2_aarch64 | rcc  | ocpi.core.platforms.xilinx19_2_aarch64      | linux-19_2-aarch64  | N/A                        | N/A        |
    | zcu102             | hdl  | ocpi.osp.xilinx.platforms.zcu102            | zynq_ultra          | xczu9eg-2-ffvb1156e        | xilinx     |
    | zcu104             | hdl  | ocpi.platform.platforms.zcu104              | zynq_ultra          | xczu7ev-2-ffvc1156e        | xilinx     |
    ~~~    
<br>

<a name="hdl-control-plane-verification-read-opencpi-magic-word"></a>

### HDL Control Plane verification: read OpenCPI Magic Word
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to verify that the implementation of the HDL Control Plane has enabled the ability for the user to read the "Magic Word" from the board. This "Magic Word" spells out "CPIxxOPEN" in hex and is a first simple validation step the the OpenCPI HDL Control Plane is functioning correctly. Furthermore, as this step only requires devmem to be present on the SD card image, it does not require that the OpenCPI run-time utilities to be cross-compilied.  

- **CRITICAL NOTE:**
    - **For this verification, the bitstream MUST be packaged into the BOOT.BIN, so that it is loaded during u-boot. Steps for including the bitstream in the BOOT.BIN are detailed below.**

Build HDL Control Plane-ONLY Assembly
1. Setup terminal for OpenCPI development
2. Build the pattern_capture_asm assembly for the zcu102 platform  
    $ cd /home/user/opencpi/projects/assets/hdl/assemblies/pattern_capture_asm directory  
    $ ocpidev build --hdl-platform zcu102
3. Once built, navigate to the container-pattern_capture.../target-zynq_ultra directory and take note of the directory and the pattern_capture_asm_zcu102_base.bit file.

Package the bitstream into the BOOT.BIN
1. In another terminal window, setup for developing with Petalinux 2019.2  
    $ source /opt/pkg/petalinux/2019.2/settings.sh
2. Change into the PetaLinux workspace directory for Control Plane ONLY development  
    $ cd /home/user/plx_zcu102_cp
3. Force the pattern_capture_asm_zcu102_base.bit bitstream to be include in the BOOT.BIN  
    - Navigate to the /home/user/plx_zcu102_cp/images/linux directory and perform the following command:  
        $ petalinux-package --boot --fsbl --fpga /home/user/opencpi/projects/assets/hdl/assemblies/pattern_capture_asm/container-pattern_capture_asm_zcu102_base/target-zynq/pattern_capture_asm_zcu102_base.bit --u-boot --force
4. Move or remove the contents from within the micro-SD card to be sure that it is empty.
5. Copy the BOOT.BIN and image.ub into an micro-SD card that is configured for FAT32 and has (1) partition.  
    - Link to partition SD-card: https://ragnyll.gitlab.io/2018/05/22/format-a-sd-card-to-fat-32linux.html  
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
6. Install the SD card into the zcu102, power on the zcu102
7. Open a serial connection to observe the successful booting of the Embedded Linux  
    $ sudo screen /dev/ttyUSB0 115200
8. Petalinux Login:  
    plx_zcu102_cp login: root  
    Password: root  

Read the OpenCPI "Magic Word" using devmem
1. Read the OpenCPI "Magic Word"  
    Perform the following commands:  
    1. root@plx_zcu102_cp:~# devmem 0xa8000000  
        Output: 0x4F70656E  
    2. root@plx_zcu102_cp:~# devmem 0xa8000004  
        Output: 0x43504900  
    3. These two output lines translate to the following:  
        https://codebeautify.org/hex-string-converter  
        Coverter Input: 435049004f70656e  
        Converter Output: CPIxxOpen  
        
Cleanup - Remove the bitstream from within the BOOT.BIN  
- Prior to moving on to the next step, update the BOOT.BIN such that NO bitstream is included, otherwise setting up for Server Mode will have issues. Server Mode is used for running Component Unit Tests during the Data Plane validation section.
    1. Navigate to the /home/user/plx_zcu102_cp/images/linux  directory and perform the following command:  
    $ petalinux-package --boot --fsbl --u-boot --force
    2. Copy the BOOT.BIN and image.ub into an micro-SD card   
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. Install the SD card into the , power on the zcu102
<br>

<a name="install-and-deploy-the-hdl-platform-with-control-plane-enabled"></a>

### Install and Deploy the HDL Platform with Control Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Setup reference directories /opt/Xilinx/ZynqReleases and /opt/Xilinx/git that are used to cross-compile the OpenCPI run-time utilities and RCC workers for the embedded ARM-based platform.
- Use the formal OpenCPI platform install and deploy commands to produce the artifacts necessary to populate an SD card.
- Verify that the Control-Plane enabled configuration successfully boots on the target platform

1. Setup the Software cross-compiler 
    - The following commands are outlined in the OpenCPI Installation Guide here:  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf (PG. 57)   
    1. ZynqReleases implementation  
        $ mkdir /home/user/2019.2-zcu102-release  
        **CRITICAL NOTE: This directory name must have the form: \<tool version>-\<bsp_name>-release**   
        $ cd /home/user/plx_zcu102_cp/images/linux    
        $ cp BOOT.BIN image.ub /home/user/2019.2-zcu102-release  
        $ cd /home/user/  
        $ tar cvfz 2019.2-zcu102-release.tar.xz 2019.2-zcu102-release  
        $ sudo cp 2019.2-zcu102-release.tar.xz /opt/Xilinx/ZynqReleases/2019.2  
        $ sudo chown -R \<user>:users /opt/Xilinx/ZynqReleases 
        - Example: sudo chown -R smith:users /opt/Xilinx/ZynqReleases   
        **NOTE: This may require adjusting the permissions for /opt/Xilinx/ZynqReleases or its subdirectories.**

2. Clean stale data in xilinx19_2_aarch64 (rcc-platform)  
    1. Remove xilinx19_2_aarch64  
        $ cd /home/user/opencpi  
        $ rm -rf cdk/xilinx19_2_aarch64  
    2. Clean xilinx19_2_aarch64  
        $ cd /home/user/opencpi/projects/core/rcc/platforms/xilinx19_2_aarch64  
        $ make clean  
        $ rm -rf lib  
    3. Rebuild core exports  
        $ cd /home/user/opencpi/projects/core  
        $ make cleanexports  
        $ make exports 

3. Install zcu102 (hdl-platform) and xilinx19_2_aarch64 (rcc-platform) 
    1. Install zcu102 (hdl-platform)  
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform zcu102  
    2. Install xilinx19_2_aarch64 (rcc-platform)    
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform xilinx19_2_aarch64 

4. Deploy: zcu102 with xilinx19_2_aarch64
    1. Execute the deployment (with "verbose")  
        $ cd /home/user/opencpi  
        $ ocpiadmin deploy -v platform xilinx19_2_aarch64 zcu102  

5. Verify the contents of the sd-card components  
**NOTE:** While your md5sum's and the guides md5sum may differ, you should verify that the two directories below contain the same md5sum's on **your** system.
    1. Check the md5sum of the sd-card components  
        $ cd /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64    
        $ md5sum BOOT.BIN    
        b86992ac2baacf8e3cf13330dfc25a8d  BOOT.BIN
        $ md5sum image.ub   
        9652b7257f94a9a33dd909afeb9d9a06  image.ub
    2. Check the md5sum of the plx_zcu102_cp  
        $ cd /home/user/plx_zcu102_cp/image/linux  
        $ md5sum BOOT.BIN  
        b86992ac2baacf8e3cf13330dfc25a8d  BOOT.BIN (MATCH)  
        $ md5sum image.ub  
        9652b7257f94a9a33dd909afeb9d9a06  image.ub (MATCH)

6. Copy files to the SD card
    1. cd /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64
    2. cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. $ cp -RLp /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/ /run/media/\<user>/\<sd-card>/ 

7. At boot-up, verify that the system is running the Control Plane configuration (plx_zcu102_cp)
    ~~~
    plx_zcu102_cp login: root
    Password:
    root@plx_zcu102_cp:~
    ~~~
<br>

<a name="hdl-control-plane-verification-test-application-pattern_capture"></a>

### HDL Control Plane verification: test application pattern_capture
---------------------------------------------------------------------------------------------------
GOAL:
- Run the HDL Control Plane application on the embedded platform
- **NOTE: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all zcu102 board variations.**

1. Use the SD card created in [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section.
2. Install the SD card into the zcu102 and apply power.
3. Establish a serial connection from the Host to the zcu102, and open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
4. Petalinux Login:  
    plx_zcu102_cp login: root  
    Password: root  
5. Mount the "opencpi" directory
    ~~~
    root@plx_zcu102_cp:~/# mkdir opencpi  
    root@plx_zcu102_cp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_zcu102_cp:~/# cd opencpi/  
    root@plx_zcu102_cp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch64     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
6. Edit the Standalone Mode scripts to load the Control Plane ONLY bitstream
    1. Make a copy of "`default_mysetup.sh`" named "`mysetup.sh`"  
        root@plx_zcu102_cp:~/opencpi# cp `default_mysetup.sh` `mysetup.sh`
    2. Edit the "`mysetup.sh`" to load the pattern_capture_asm bitstream:  
        FROM:  
        if ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/testbias_$HDL_PLATFORM\_base.bitz; then  
        
        TO:  
        if ocpihdl load -d $OCPI_DEFAULT_HDL_DEVICE $OCPI_CDK_DIR/artifacts/pattern_capture_asm_$HDL_PLATFORM\_base.bitz; then
7. Setup the embedded platform environment for Standalone Mode by sourcing the Standalone Mode setup script `mysetup.sh`
    1. root@plx_zcu102_cp:~/# cd /opencpi
    2. root@plx_zcu102_cp:~/opencpi# source /home/root/opencpi/mysetup.sh
8. Run application  
    % cd /home/root/opencpi/applications/  
    % export OCPI_LIBRARY_PATH=../artifacts  
    % ocpirun -v -d pattern_capture.xml
9. stdout of screen session:
    ~~~
    root@plx_zcu102_cp:~/opencpi# source /home/root/opencpi/mysetup.sh
    Attempting to set time from time.nist.gov
    rdate: bad address 'time.nist.gov'
    ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
    Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
    Running login script.
    OCPI_CDK_DIR is now /run/media/mmcblk0p1/opencpi.
    OCPI_ROOT_DIR is now /run/media/mmcblk0p1/opencpi/...
    Executing /home/root/.profile.
    No reserved DMA memory found on the linux boot command line.
    [   96.189646] opencpi: loading out-of-tree module taints kernel.
    [   96.197861] opencpi: dma_set_coherent_mask failed for device ffffffc8793dcc00
    [   96.205046] opencpi: get_dma_memory failed in opencpi_init, trying fallback
    [   96.212036] NET: Registered protocol family 12
    Driver loaded successfully.
    OpenCPI ready for zynq.
    Loading bitstream
    Bitstream loaded successfully
    Discovering available containers...
    Available containers:
     #  Model Platform            OS     OS-Version  Arch     Name
     0  hdl   zcu102                                          PL:0
     1  rcc   xilinx19_2_aarch64  linux  19_2        aarch64  rcc0
    % cd /home/root/opencpi/applications/
    % export OCPI_LIBRARY_PATH=../artifacts
    % ocpirun -v -d pattern_capture.xml
    Available containers are:  0: PL:0 [model: hdl os:  platform: zcu102], 1: rcc0 [model: rcc os: linux platform: xilinx19_2_aarch64]
    Actual deployment is:
      Instance  0 pattern_v2 (spec ocpi.assets.util_comps.pattern_v2) on hdl container 0: PL:0, using pattern_v2/a/pattern_v2 in ../artifacts/pattern_capture_asm_zcu102_base.bitz dated Thu May 13 09:16:58 2021
      Instance  1 capture_v2 (spec ocpi.assets.util_comps.capture_v2) on hdl container 0: PL:0, using capture_v2/a/capture_v2 in ../artifacts/pattern_capture_asm_zcu102_base.bitz dated Thu May 13 09:16:58 2021
    Application XML parsed and deployments (containers and artifacts) chosen [0 s 33 ms]
    Application established: containers, workers, connections all created [0 s 16 ms]
    Dump of all initial property values:
    Property   0: pattern_v2.dataRepeat = "true" (cached)
    Property   1: pattern_v2.numMessagesMax = "5" (parameter)
    Property   2: pattern_v2.messagesToSend = "5"
    Property   3: pattern_v2.messagesSent = "0"
    Property   4: pattern_v2.dataSent = "0"
    Property   5: pattern_v2.numDataWords = "15" (parameter)
    Property   6: pattern_v2.numMessageFields = "2" (parameter)
    Property   7: pattern_v2.messages = "{4,251},{8,252},{12,253},{16,254},{20,255}" (cached)
    Property   8: pattern_v2.data = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14" (cached)
    Property  20: capture_v2.stopOnFull = "true" (cached)
    Property  21: capture_v2.metadataCount = "0"
    Property  22: capture_v2.dataCount = "0"
    Property  23: capture_v2.numRecords = "256" (parameter)
    Property  24: capture_v2.numDataWords = "1024" (parameter)
    Property  25: capture_v2.numMetadataWords = "4" (parameter)
    Property  26: capture_v2.metaFull = "false"
    Property  27: capture_v2.dataFull = "false"
    Property  28: capture_v2.stopZLMOpcode = "0" (cached)
    Property  29: capture_v2.stopOnZLM = "false" (cached)
    Property  30: capture_v2.stopOnEOF = "true" (cached)
    Property  31: capture_v2.totalBytes = "0"
    Property  32: capture_v2.metadata = "{0}"
    Property  33: capture_v2.data = "0"
    Application started/running [0 s 7 ms]
    Waiting for application to finish (no time limit)
    Application finished [0 s 0 ms]
    Dump of all final property values:
    Property   0: pattern_v2.dataRepeat = "true" (cached)
    Property   2: pattern_v2.messagesToSend = "0"
    Property   3: pattern_v2.messagesSent = "5"
    Property   4: pattern_v2.dataSent = "15"
    Property   7: pattern_v2.messages = "{4,251},{8,252},{12,253},{16,254},{20,255}" (cached)
    Property   8: pattern_v2.data = "0,1,2,3,4,5,6,7,8,9,10,11,12,13,14" (cached)
    Property  20: capture_v2.stopOnFull = "true" (cached)
    Property  21: capture_v2.metadataCount = "5"
    Property  22: capture_v2.dataCount = "15"
    Property  26: capture_v2.metaFull = "false"
    Property  27: capture_v2.dataFull = "false"
    Property  28: capture_v2.stopZLMOpcode = "0" (cached)
    Property  29: capture_v2.stopOnZLM = "false" (cached)
    Property  30: capture_v2.stopOnEOF = "true" (cached)
    Property  31: capture_v2.totalBytes = "60"
    Property  32: capture_v2.metadata = "{4211081220,2747010701,2747010701,30},{4227858440,2747010701,2747010701,30},{4244635660,2747010916,2747010916,30},{4261412880,2747011131,2747011131,30},{4278190100,2747011345,2747011345,30},{0}"
    Property  33: capture_v2.data = "0,0,1,0,1,2,0,1,2,3,0,1,2,3,4,0"
    ~~~  
10. Verify that the data has successfully passed through the application by consulting the /home/user/projects/assets/applications/pattern_capture/README file
<br>

<a name="enable-opencpi-hdl-data-plane"></a>

## Enable OpenCPI HDL Data Plane
---------------------------------------------------------------------------------------------------
<a name="configure-ps-for-opencpi-data-plane"></a>

### Configure PS for OpenCPI Data Plane
---------------------------------------------------------------------------------------------------
GOALS:
- Modify the PS core IP from the [Enable OpenCPI HDL Control Plane](#enable-opencpi-hdl-control-plane) section, to add the ports necessary to support enabling the OpenCPI Data Plane
- Re-generate the PS core IP output products for updating the Petalinux project to create an SD-Card Linux image
- Build, Run/Verify the "canary" Data Plane application, "testbias"
- Build, Run/Verify another application which requires the Data Plane, i.e. FSK filerw

**NOTE:** All default settings of the PS core IP as a result of applying the 'Preset', are accepted, unless specified in the steps below.  

1. These steps continue with the completion of the [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane) section.   
2. Open the Vivado "ps8_zcu102_ocpi_cp" project, File -> Project -> Save As ->  "ps8_zcu102_ocpi_dp" -> Ensure that the "Create project subdirectory" box is checked, and confirm the "Project Location" is in the desired directory (/home/user/) level prior to clicking "OK". This will provide a seperate project for configuring the PS core IP to support the DP only OSP. 
    - **CRITICAL NOTE: This will give the developer the flexability to create subdirectories for each of the main development sections (['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)), [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane), [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)), and go back to previous sections of the guide if needed.**
3. Enable the slave high performance Data Plane ports of the PS core IP
    1. Open the Block Design
    2. Double-click the zynq_ultra_ps_e_0 IP Block
    3. Click on the PS-PL Configuration
        1. "PS-PL Interface" -> "Slave Interface" -> Check the following AXI HP Ports:
            1. "AXI HP0 FPD"
            2. "AXI HP1 FPD"
            3. "AXI HP2 FPD"
            4. "AXI HP3 FPD"
        2. From the same location expand each of the enabled AXI HP Ports to select a 64 bit "AXI HP FPD Data Width"
        3. Select "OK"
4. Externalize the newly created aclk ports
    1. Select "saxihp0_fpd_aclk" port, then press Ctrl + T
    2. Select "saxihp1_fpd_aclk" port, then press Ctrl + T
    3. Select "saxihp2_fpd_aclk" port, then press Ctrl + T
    4. Select "saxihp3_fpd_aclk" port, then press Ctrl + T
5. Confirm that each of the newly created externalized aclk ports have a 100 MHz Frequency by selecting each of the ports and locating the "External Port Properties" window and validating that the frequency is: "Frequency (MHz): 100"
6. "Validate Design" (F6)
7. Flow Navigator window → IP INTEGRATOR → Generate Block Design  
    **CRITCAL NOTE:**  
    **Recall from the ['Preset' Board Design Files (BDF)](#preset-board-design-files-(bdf)) section, that "Create HDL Wrapper" was performed, because it is a prerequiste for performing "Export Hardware".**  
    1. Performing "Generate Block Design", should automatically update the HDL Wrapper file.
8. After the Generate Block Design is complete, export the hardware by selecting
    1. File → Export → Export Hardware
    2. DO NOT check the box for "Include Bitstream"
    2. Confirm the "Export to" path is correct
    3. Select "OK"
9. The design should look as follows:  
        ![Data Plane BD](./images/data_plane_BD.png)  
<br>

<a name="petalinux-workspace-for-data-plane"></a>

### PetaLinux workspace for Data Plane
---------------------------------------------------------------------------------------------------
1. Return to the [Petalinux Build](#petalinux-build) section, to update the SD card and confirm that the Embedded Linux will boot properly.  
2. Create a PetaLinux workspace for the development of Data Plane SD card image  
    $ /home/user/plx_zcu102_dp  
    **CRITICAL NOTE: When performing petalinux-package in the [Petalinux Build](#petalinux-build) section, do not include the --fpga argument as this section has not produce a bitstream.**  
<br>

<a name="configure-hdl-primitive-for-data-plane"></a>

### Configure HDL Primitive for Data Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Edit the OpenCPI HDL primitive library source to support the High-Performance (HP) ports that were made available to the Zynq Processing System above.  

    **CODE BLOCK: The "finalized" code block of various files mentioned in this section can be found at:**
    - **ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102** 

1. At the start of this effort, perform a clean within the OSP directory to ensure that no stale files exist  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
    $ ocpidev clean
2. To avoid stale content, remove the current PS core IP before copying over the updated version  
    $ cd /home/user/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102  
    $ rm -rf preset_config_wrapper_zynq_ultra_ps_e_0_0/
3. From [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane), copy the updated preset_config_wrapper_zynq_ultra_ps_e_0_0 directory into the ocpi.osp.xilinx HDL primitive directory  
    $ cp -rf /home/user/ps8_zcu102_ocpi_dp/ps8_zcu102_ocpi_dp.srcs/sources_1/bd/preset_config_wrapper/ip/preset_config_wrapper_zynq_ultra_ps_e_0_0/ /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/primitives/zynq_ultra_zcu102  
5. Edit the zynq_ultra_zcu102_pkg.vhd file to include the newly enabled HP Slave Data Plane ports 
    1. Uncomment the s_axi_hp_in port
    2. Uncomment the s_axi_hp_out port
6. Edit the zynq_ultra_zcu102_ps.vhd file to enable the newly enabled HP Slave Data Plane ports.
    1. Uncomment the s_axi_hp_in port
    2. Uncomment the s_axi_hp_out port
    3. Uncomment all other ports which included:
        1. saxihp0, saxihp1, saxihp2, saxihp3
        2. saxigp2, saxigp3, saxigp4, saxigp5
    4. Uncomment the "gs: for i in 0 to C_S_AXI_HP_COUNT-1 generate" code block
<br>

<a name="configure-hdl-platform-worker-for-data-plane"></a>

### Configure HDL Platform Worker for Data Plane
---------------------------------------------------------------------------------------------------
GOAL:
- Edit the HDL Platform Worker files in order to implement the High-Performance (HP) ports that were made available to the Zynq MP Processing System.  

    **CODE BLOCK: The codeblock of various files mentioned in this section can be found at:**
    - **ocpi.osp.xilinx/guide/zcu102/Enable_OpenCPI_Data-Plane/Platform-Worker/** 

1. Edit the zcu102.xml file
    1. Include the following specproperty to enable the 64-bit data lines
        ~~~
        <specproperty name='sdp_width' value='2'/>
        ~~~
    2. Uncomment the ```<sdp name='zynq_ultra' master='true' count='4'/>```
    3. Uncomment the following Property names: axi_error, sdpDropCount
    4. Uncomment the following specproperties: nSwitches
        - Ensure that there are eight signals for each of the switches[7:0]
        - Rename the switches(n) to map to the proper platform signal GPIO_PB_SW(n)  
        ```  <signal name='switches(#)' platform="GPIO_DIP_SW#"/>```    
2. Edit the zcu102.vhd file:
    1. Uncomment the newly created HP ports
    2. Uncomment all ports **EXCEPT**
        - dbgState ports
    3. Edit the following constant
        FROM:
        ~~~
        g : for i in 0 to C_M_AXI_HP_COUNT-1 generate
        ~~~
        TO:
        ~~~
        g : for i in 0 to C_S_AXI_HP_COUNT-1 generate
        ~~~
3. Edit the zcu.xdc file to include the DIP Switches
    1. Uncomment all of the GPIO DIP Switches: "GPIO_DIP_SW0" - "GPIO_DIP_SW7"
<br>

<a name="build-the-hdl-platform-with-data-plane-enabled"></a>

### Build the HDL Platform with Data Plane enabled
---------------------------------------------------------------------------------------------------
1. Build the HDL Platform Worker and "base" Platform Configuration
    $ cd /home/user/projects/osps/ocpi.osp.xilinx
    $ ocpidev build --hdl-platform zcu102
<br>

<a name="undo-edits-made-to-validate-hdl-control-plane"></a>

### Undo edits made to validate HDL Control Plane
GOAL:
- Initially in support of validating the HDL platform for Control Plane ONLY, several scripts were modified to build and deploy the "canary" Control Plane bitstream (pattern_capture). The purpose of this section is to revert those changes such that the "canary" Data Plane bitstream (testbias) will be installed (i.e. built) and deployed for the targetted HDL platform.

1. git checkout tools/scripts/deploy-platform.sh
2. git checkout tools/scripts/export-platform-to-framework.sh
3. git checkout tools/scripts/ocpiadmin.sh
<br>

<a name="install-and-deploy-the-hdl-platform-with-data-plane-enabled"></a>

### Install and Deploy the HDL Platform with Data Plane enabled
---------------------------------------------------------------------------------------------------
GOALS:
- Setup reference directories /opt/Xilinx/ZynqReleases and /opt/Xilinx/git that are used to cross-compile the OpenCPI run-time utilities and RCC workers for the embedded ARM-based platform.
- Clean stale data that may be present in the xilinx19_2_aarch64 rcc-platform so fresh sd-card contents (BOOT.BIN and image.ub) are implemented at the deploy stage.
- Use the formal OpenCPI platform install and deploy commands to produce the artifacts necessary to populate an SD card.
- Verify that the Data-Plane enabled configuration has been successfully implemented

    **NOTE:** The ZynqReleases directory should have been setup in the [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section. In this section we will be replacing the CP BOOT.BIN and image.ub with the updated DP BOOT.BIN and image.ub that was created in the [PetaLinux workspace for Data Plane](petalinux-workspace-for-data-plane) section. The git directory that was created in the  [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section should be unaltered as no changes need to be implemented.  

1. Setup the Software cross-compiler 
    - The following commands are outlined in the OpenCPI Installation Guide here:  
    https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf (PG. 57)   
    1. ZynqReleases implementation  
        $ cd /home/user/plx_zcu102_dp/images/linux  
        $ cp BOOT.BIN image.ub /home/user/2019.2-zcu102-release    
        $ cd /home/user/    
        $ tar cvfz 2019.2-zcu102-release.tar.xz 2019.2-zcu102-release    
        $ sudo cp 2019.2-zcu102-release.tar.xz /opt/Xilinx/ZynqReleases/2019.2    
        $ sudo chown -R \<user>:users /opt/Xilinx/ZynqReleases   
        - Example: sudo chown -R smith:users /opt/Xilinx/ZynqReleases   
        **NOTE:** This may require adjusting the permissions for /opt/Xilinx/ZynqReleases or its subdirectories.        

2. Clean stale data in xilinx19_2_aarch64 (rcc-platform)  
    1. Remove xilinx19_2_aarch64  
        $ cd /home/user/opencpi  
        $ rm -rf cdk/xilinx19_2_aarch64  
    2. Clean xilinx19_2_aarch64  
        $ cd /home/user/opencpi/projects/core/rcc/platforms/xilinx19_2_aarch64  
        $ make clean  
        $ rm -rf lib 
    3. Rebuild core exports  
        $ cd /home/user/opencpi/projects/core  
        $ make cleanexports  
        $ make exports  
    
3. Install zcu102 (hdl-platform) and xilinx19_2_aarch64 (rcc-platform))  
    1. Install zcu102 (hdl-platform)  
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform zcu102  
    2. Install xilinx19_2_aarch64 (rcc-platform)   
        $ cd /home/user/opencpi  
        $ ocpiadmin install platform xilinx19_2_aarch64  

4. Deploy: zcu102 (hdl-platform) with platform xilinx19_2_aarch64 (rcc-platform)
    1. Execute deployment (with "verbose")   
        $ cd /home/user/opencpi  
        $ ocpiadmin deploy -v platform xilinx19_2_aarch64 zcu102  

5. Verify the contents of the sd-card components  
**NOTE:** While your md5sum's and the guides md5sum may differ, you should verify that the two directories below contain the same md5sum's on **your** system.
    1. Check the md5sum of the sd-card components  
        $ cd /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64    
        $ md5sum BOOT.BIN  
        071718a4d1c6648cfe62dc093fc2a499  BOOT.BIN  
        $ md5sum image.ub  
        9776460860e7242c004b2f3cea0d6cc2  image.ub
    2. Check the md5sum of the plx_zcu102_dp  
        $ cd /home/user/plx_zcu102_dp/image/linux  
        $ md5sum BOOT.BIN  
        071718a4d1c6648cfe62dc093fc2a499  BOOT.BIN (MATCH)  
        $ md5sum image.ub  
        9776460860e7242c004b2f3cea0d6cc2  image.ub (MATCH)

6. Copy files to the SD card
    1. cd /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64
    2. cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/
    3. $ cp -RLp /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/ /run/media/\<user>/\<sd-card>/

7. At boot-up, verify that the system is running the Data Plane configuration (plx_zcu102_dp)
    ~~~
    plx_zcu102_dp login: root
    Password:
    root@plx_zcu102_dp:~
    ~~~
<br>

<a name="hdl-data-plane-verification:-testbias-application"></a>

### HDL Data Plane verification: testbias application
---------------------------------------------------------------------------------------------------
GOAL:
- Successfully execute the "canary" HDL Data Plane application on the embedded platform."Success" is defined as the application ran to completion, and the md5sum of the input and output data of the testbias application match, when no bias is being applied to the data, i.e. bias worker property, biasValue=0.

**NOTE: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all zcu102 board variations.**

1. Use the SD card as a result of the work performed in [Install and Deploy the HDL Platform with Data Plane enabled](#install-and-deploy-the-hdl-platform-with-data-plane-enabled)
2. Install the SD card into the  and apply power.
3. Establish a serial connection from the Host to the , open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
4. Petalinux Login:  
    plx_zcu102_dp login: root  
    Password: root  
5. Mount the "opencpi" directory
    ~~~
    root@plx_zcu102_dp:~/# mkdir opencpi  
    root@plx_zcu102_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_zcu102_dp:~/# cd opencpi/  
    root@plx_zcu102_dp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch64     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
6. Setup the embedded platform environment for Standalone Mode by sourcing the appropriate setup script, i.e. `mysetup.sh`   
    root@plx_zcu102_dp:~/opencpi# cp default_mysetup.sh `mysetup.sh`  
    root@plx_zcu102_dp:~/opencpi# source /home/root/opencpi/mysetup.sh  
7. Run application: testbias  
    % cd /home/root/opencpi/applications  
    % export OCPI_LIBRARY_PATH=../artifacts:../xilinx19_2_aarch64/artifacts   
    % export OCPI_DMA_CACHE_MODE=0   
    Check that testbias is functioning as expected by verifying that the input and output are equal when assigning a testbias of zero 0 (no change).  
    % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml  
8. stdout of screen session:
    ~~~
    root@plx_zcu102_dp:~/# mkdir opencpi
    root@plx_zcu102_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/
    root@plx_zcu102_dp:~/# cd opencpi/
    root@plx_zcu102_dp:~/opencpi# cp default_mysetup.sh mysetup.sh    
    root@plx_zcu102_dp:~/opencpi# source /home/root/opencpi/mysetup.sh
    Attempting to set time from time.nist.gov
    rdate: bad address 'time.nist.gov'
    ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
    Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
    Running login script.
    OCPI_CDK_DIR is now /run/media/mmcblk0p1/opencpi.
    OCPI_ROOT_DIR is now /run/media/mmcblk0p1/opencpi/...
    Executing /home/root/.profile.
    No reserved DMA memory found on the linux boot command line.
    opencpi: loading out-of-tree module taints kernel.
    NET: Registered protocol family 12
    Driver loaded successfully.
    OpenCPI ready for zynq.
    Loading bitstream
    Bitstream loaded successfully
    Discovering available containers...
    Available containers:
    #  Model Platform            OS     OS-Version  Arch     Name
    0  hdl   zcu102                                   PL:0
    1  rcc   xilinx19_2_aarch64  linux  19_2        aarch32  rcc0
    % pwd
    /run/media/mmcblk0p1/opencpi
    % cd /home/root/opencpi/applications/
    % export OCPI_LIBRARY_PATH=../artifacts:../xilinx19_2_aarch64/artifacts
    % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml
    Available containers are:  0: PL:0 [model: hdl os:  platform: zcu102], 1: rcc0 [model: rcc os: linux platform: xilinx19_2_aarch64]
    Actual deployment is:
      Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in ../xilinx19_2_aarch64/artifacts/ocpi.core.file_read.rcc.0.xilinx19_2_aarch64.so dated Thu Apr  8 11:22:04 2021
      Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in ../artifacts/testbias_zcu102_base.bitz dated Thu Apr  8 11:22:06 2021
      Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in ../xilinx19_2_aarch64/artifacts/ocpi.core.file_write.rcc.0.xilinx19_2_aarch64.so dated Thu Apr  8 11:22:04 2021
    Application XML parsed and deployments (containers and artifacts) chosen [0 s 75 ms]
    Application established: containers, workers, connections all created [0 s 10 ms]
    Dump of all initial property values:
    Property   0: file_read.fileName = "test.input" (cached)
    Property   1: file_read.messagesInFile = "false" (cached)
    Property   2: file_read.opcode = "0x0" (cached)
    Property   3: file_read.messageSize = "0x10"
    Property   4: file_read.granularity = "0x4" (cached)
    Property   5: file_read.repeat = "false"
    Property   6: file_read.bytesRead = "0x0"
    Property   7: file_read.messagesWritten = "0x0"
    Property   8: file_read.suppressEOF = "false"
    Property   9: file_read.badMessage = "false"
    Property  16: bias.biasValue = "0x0" (cached)
    Property  20: bias.test64 = "0x0"
    Property  31: file_write.fileName = "test.output" (cached)
    Property  32: file_write.messagesInFile = "false" (cached)
    Property  33: file_write.bytesWritten = "0x0"
    Property  34: file_write.messagesWritten = "0x0"
    Property  35: file_write.stopOnEOF = "true" (cached)
    Property  39: file_write.suppressWrites = "false"
    Property  40: file_write.countData = "false"
    Property  41: file_write.bytesPerSecond = "0x0"
    Application started/running [0 s 2 ms]
    Waiting for application to finish (no time limit)
    Application finished [0 s 50 ms]
    Dump of all final property values:
    Property   0: file_read.fileName = "test.input" (cached)
    Property   1: file_read.messagesInFile = "false" (cached)
    Property   2: file_read.opcode = "0x0" (cached)
    Property   3: file_read.messageSize = "0x10"
    Property   4: file_read.granularity = "0x4" (cached)
    Property   5: file_read.repeat = "false" (cached)
    Property   6: file_read.bytesRead = "0xfa0"
    Property   7: file_read.messagesWritten = "0xfa"
    Property   8: file_read.suppressEOF = "false" (cached)
    Property   9: file_read.badMessage = "false"
    Property  16: bias.biasValue = "0x0" (cached)
    Property  20: bias.test64 = "0x0" (cached)
    Property  31: file_write.fileName = "test.output" (cached)
    Property  32: file_write.messagesInFile = "false" (cached)
    Property  33: file_write.bytesWritten = "0xfa0"
    Property  34: file_write.messagesWritten = "0xfa"
    Property  35: file_write.stopOnEOF = "true" (cached)
    Property  39: file_write.suppressWrites = "false" (cached)
    Property  40: file_write.countData = "false" (cached)
    Property  41: file_write.bytesPerSecond = "0x14018"
    ~~~

9. Verify that the data has successfully passed through the application by performing an m5sum on the input and output data files with bias effectively disabled, by setting the biasValue=0
    Compare the md5sum of both test.input and test.output  
        % md5sum test.*  
    The Output should be as follows:
    ~~~
    % md5sum test.*
    2934e1a7ae11b11b88c9b0e520efd978  test.input
    2934e1a7ae11b11b88c9b0e520efd978  test.output
    ~~~
    This shows that with a bias=0 (no change in data) that the input matches the output and the testbias application is working as it should.  
<br>

<a name="hdl-data-plane-verification:-test-fsk-application-filerw-mode"></a>

### **HDL Data Plane verification: test FSK application filerw mode**
---------------------------------------------------------------------------------------------------
GOAL:
- (OPTIONAL) Execute another standard application which relys upon the HDL Data Plane

**NOTE**: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the verfication test results of all zcu102 board variations.

This section is outlined in the following location: /home/user/opencpi/projects/assets/applications/FSK/doc/FSK_app.tex. This document covers the FSK "txrx" execution mode however, the "filerw" execution mode will be used in this section to validate the implementation of the Data Plane. To retrieve the contents of the FSK_app.tex file install rubber and run the following command:  
$ rubber -d FSK_App_Getting_Started_Guide.tex  
$ envince FSK_App_Getting_Started_Guide.pdf  

Provided here will be a setup guide to building and deploying the FSK application filerw mode:  
**NOTE: $ will signal that the command is performed on the Development host, % will signal that the command is performed on the Target-platform**  

1. Build the FSK Application Executable and copy it into the SD-Card  
    $ cd /home/user/opencpi/projects/assets/applications/FSK  
    $ ocpidev build --rcc-platform xilinx19_2_aarch64  
    $ cd ..  
    $ cp -rf FSK/ /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/applications 
2. Build the fsk_filerw hdl assembly and copy it into the SD-Card 
    $ cd /home/user/opencpi/projects/assets/hdl/assemblies/fsk_filerw 
    $ ocpidev build --hdl-platform zcu102  
    $ cd container-fsk_filerw_zcu102_base/target-zynq_ultra 
    $ cp fsk_filerw_zcu102_base.bitz /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/artifacts  
3. Implement missing .so files into the sd-card xilinx19_2_aarch64 artifacts directory  
    **NOTE: The following files are needed based off of the /home/user/opencpi/projects/assets/applications/FSK/app_fsk_filerw.xml file under RCC Components. The ocpi.core.file_read is already given but the ocpi.assets.dsp_comps.baudTracking and ocpi.assets.dsp_comps.real_digitizer components need to be supplemented. Check the /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/xilinx19_2_aarch64/artifacts to see what is included**    
    1. Copy the Baudtracking_simple.so into the SD-Card xilinx19_2_aarch64 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/Baudtracking_simple.rcc/target-xilinx19_2_aarch64  
        $ cp Baudtracking_simple.so /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/xilinx19_2_aarch64/artifacts/  
    2. Copy the real_digitizer.so into the SD-Card xilinx19_2_aarch64 artifacts directory  
        $ cd /home/user/opencpi/projects/assets/components/dsp_comps/real_digitizer.rcc/target-xilinx19_2_aarch64  
        $ cp real_digitizer.so /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/xilinx19_2_aarch64/artifacts/  
4. Copy the newly implemented file onto the SD-Card
    $ cp -RLp /home/user/opencpi/cdk/zcu102/sdcard-xilinx19_2_aarch64/opencpi/ /run/media/\<user>/\<sd-card>/
5. Install SD into zcu102 and apply power
6. Connect to the Platform  
    \# sudo screen /dev/tty/USB0 115200  
    Petalinux Login:  
    plx_zcu102_dp login: root  
    Password: root 
7. Mount the "opencpi" directory
    ~~~
    root@plx_zcu102_dp:~/# mkdir opencpi  
    root@plx_zcu102_dp:~/# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    root@plx_zcu102_dp:~/# cd opencpi/  
    root@plx_zcu102_dp:~/opencpi# ls
    COPYRIGHT              applications           default_mynetsetup.sh  opencpi-setup.sh       
    system.xml             zynq_setup.sh          LICENSE.txt            artifacts              
    default_mysetup.sh     release                xilinx19_2_aarch64     zynq_setup_common.sh
    VERSION                default-system.xml     mysetup.sh             scripts                
    zynq_net_setup.sh
    ~~~
8. Setup the embedded platform environment for Standalone Mode by sourcing the appropriate setup script, i.e. `mysetup.sh`  
    root@plx_zcu102_dp:~/opencpi# cp `default_mysetup.sh` `mysetup.sh`  
    root@plx_zcu102_dp:~/opencpi# source /home/root/opencpi/mysetup.sh  
9. Setup the library path to the artifacts  
    % cd /home/root/opencpi/applications/FSK  
    % export OCPI_LIBRARY_PATH=/home/root/opencpi/artifacts:/home/root/opencpi/xilinx19_2_aarch64/artifacts  
    % export OCPI_DMA_CACHE_MODE=0
10. Run application: FSK filerw  
    % ./target-xilinx19_2_aarch64/FSK filerw  
    ~~~
    % ./target-xilinx19_2_aarch64/FSK filerw
    Application properties are found in XML file: app_fsk_filerw.xml
    App initialized.
    App started.
    Waiting for done signal from file_write.
    real_digitizer: sync pattern 0xFACE found
    App stopped/finished.
    Bytes to file : 8950
    TX FIR Real Peak       = 31014
    Phase to Amp Magnitude = 20000
    RP Cordic Magnitude    = 16805
    RX FIR Real Peak       = 15823
    Application complete
    ~~~
11. In order to see the output of the execution of this application the user needs to copy over the output file located in /run/media/mmcblk0p1/opencpi/applications/FSK/odata/out_app_fsk_filerw.bin over to their development host. In order to do this:
    1. Move the SD to the development host, or
    2. Implement an IP address for the target-platform and scp the out_app_fsk_filerw.bin to the development host.
        1. In order to configure eth0, the [zcu102 Ethernet Bug fix for PetaLinux](#zcu102-ethernet-bug-fix-for-petalinux) must be implemented into the petalinux build-flow.
        2. Implement IP-Address (If it was not setup in [Static IP Address Setup](#static-ip-address-setup))   
            % ifconfig eth0 down  
            % ifconfig eth0 add \<Create Valid ip-address> netmask 255.255.255.0  
            % ifconfig eth0 up  
        3. Secure Copy the output file out_app_fsk_filerw.bin that was produced  
            % cd /run/media/mmcblk0p1/opencpi/applications/FSK/odata  
            % scp out_app_fsk_filerw.bin \<host>@\<host-ip>:\<path>  

12. View the results of the output file      
    $ eog \<path>/out_app_fsk_filerw.bin  
    ![Oriole](./images/oriole.png)  
<br>

---------------------------------------------------------------------------------------------------
<a name="opencpi-component-unit-testing"></a>

## OpenCPI Component Unit Testing
---------------------------------------------------------------------------------------------------

<a name="server-mode-setup"></a>

### Server Mode setup
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to enable the user with the ability to setup the Server Mode. Success of this section is the ability to utilize the ocpiremote utility that enables the Server Mode and provides the ability to load bitstreams from the Client-side (Host) to the Server-side (embedded device).

Enable the zcu102 for remote containers by ensuring its system.xml contains \<remote load='1'> and \<socket load='1'> :
~~~
<opencpi>
  <container>
    <rcc load='1'/>
    <remote load='1'/>
    <hdl load='1'>
     <device name='PL:0' platform='zcu102'/>
    </hdl>
  </container>
  <transfer smbsize='128K'>
    <pio load='1' smbsize='10M'/>
    <dma load='1'/>
    <socket load='1'/>
  </transfer>
</opencpi>
~~~

Server-side setup:
1. Establish a serial connection from the Host to the zcu102, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
2. Petalinux Login:  
    plx_zcu102_dp login:root  
    Password:root 
3. Network setup: (If it was not setup in [Static IP Address Setup](#static-ip-address-setup))  
- In order to configure eth0, the [zcu102 Ethernet Bug fix for PetaLinux](#zcu102-ethernet-bug-fix-for-petalinux) must be implemented into the petalinux build-flow.  
    $ ifconfig eth0 down  
    $ ifconfig eth0 add \<Vailid ip-address> netmask 255.255.255.0   
    $ ifconfig eth0 up

Client-side setup:
1. Change to your Opencpi directory:  
    $ cd /home/user/opencpi  
2. Export some Opencpi enviornment variables to discover the remote server:  
    $ export OCPI_SERVER_ADDRESSES=\<Valid ip-address>:\<Valid port>   
    $ export OCPI_SOCKET_INTERFACE=\<Valid socket>  
        "Valid socket" is the name of the Ethernet interface of the development host which can communicate with the zcu102.
3. Load "sandbox" onto the server:  
    $ ocpiremote load -s xilinx19_2_aarch64 -w zcu102  
    Standard Out:
    ~~~
    $ ocpiremote load -s xilinx19_2_aarch64 -w zcu102
    Preparing remote sandbox...
    Fri Mar 26 13:09:23 UTC 2021
    Creating server package...
    Sending server package...
    Server package sent successfully.  Getting status (no server expected to be running):
    Executing remote configuration command: status
    No ocpiserve appears to be running: no pid file        
    ~~~
4. Start the Server-Mode:  
    **NOTE**: Due to issues seen within the framework, start the server mode with the OCPI_DMA_CACHE_MODE enviornment variable set to 0. This will disable a DMA feature that was added in support of the zynq family but has shown problems when implementing it on the zynq ultrascale+ family.  
    $ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0  
    Standard Out:  
    ~~~
    $ ocpiremote start -b -e OCPI_DMA_CACHE_MODE=0
    Executing remote configuration command: start -B
    Reloading kernel driver: 
    Loading opencpi bitstream
    The driver module is not loaded. No action was taken.
    No reserved DMA memory found on the linux boot command line.
    Driver loaded successfully.
    PATH=/home/root/sandbox/xilinx19_2_aarch64/bin:/home/root/sandbox/xilinx19_2_aarch64/sdk/bin:/usr/bin:/bin
    LD_LIBRARY_PATH=xilinx19_2_aarch64/sdk/lib
    VALGRIND_LIB=
    nohup ocpiserve -v -p 12345 > 20210515-124523.log
    Server (ocpiserve) started with pid: 2374.  Initial log is:
    Discovery options:  discoverable: 0, loopback: 0, onlyloopback: 0
    Container server at <ANY>:12345
      Available TCP server addresses are:
        On interface eth0: 192.168.0.10:12345
    Artifacts stored/cached in the directory "artifacts", which will be retained on exit.
    Containers offered to clients are:
       0: PL:0, model: hdl, os: , osVersion: , platform: zcu102
       1: rcc0, model: rcc, os: linux, osVersion: 19_2, platform: xilinx19_2_aarch64
    --- end of server startup log success above
    ~~~
5. Firewall Error:  
     - If you are presented with the following error while attempting to run any of the Unit-Tests you may need to disable or reconfigure your firewall:
    ~~~
    OCPI( 2:991.0828): Exception during application shutdown: error reading from container server "": EOF on socket read
    Exiting for exception: error reading from container server "": EOF on socket read
    ~~~  
    You can disable your firewall until it is rebooted with the two following commands:  
    $ sudo systemctl disable firewalld  
    $ sudo systemctl mask --now firewalld  
<br>

<a name="build-and-run"></a>

### Build and Run
---------------------------------------------------------------------------------------------------
GOAL:
- The goal of this section is to build and run the framework provided Component Unit Tests. Success of this section is to perform all of the Component Unit Tests making sure that each of the tests "pass" or "fail" as outlined in the [Table of results](#table-of-results) section. That table compares the 'pass and fail' results compared to the zcu104. Please refer to the [Table of results](#table-of-results) section to ensure that testing behavior is consistent.

**NOTE: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the Component unit test results of all component unit tests for each of the zcu102 board variations.**

Build the Component Unit Tests (In parallel) on the Development Host
1. Build the core Component Unit Tests. (May take several hours)  
    Open a new terminal window  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/core  
    $ ocpidev build test --hdl-platform zcu102 --rcc-platform xilinx19_2_aarch64 --rcc-platform centos7  
2. Build the assets component unit tests (May take several hours)  
    Open a new terminal window  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/assets  
    $ ocpidev build test --hdl-platform zcu102 --rcc-platform xilinx19_2_aarch64 --rcc-platform centos7  
3. Build the assets_ts component unit tests (May take several hours)  
    Open a new terminal window  
    $ cd /home/user/opencpi  
    $ source cdk/opencpi-setup.sh -s  
    $ cd /home/user/opencpi/projects/assets_ts  
    $ ocpidev build test --hdl-platform zcu102 --rcc-platform xilinx19_2_aarch64 --rcc-platform centos7  

Run the Component Unit Tests (Sequentially)
- **NOTE: To ensure the best test coverage, this section is very manually intensive. The user must traverse into each \<unit>.test directory of each component library and perform "runonly" followed by "verify" of each of the tests. Some of these tests are known to fail. A chart is provided in the [Tables of results](#test-results-table) below that outlines the expected outcome for each of these tests (as of v2.1.0).**
- Chapter 13 of https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf, details the various "make" calls used in these steps
- [Server Mode setup](#server-mode-setup) was used to run the Component Unit Tests because it has proven to yield to most consistent results

    1. Run the core unit tests  
        $ cd /home/user/opencpi/projects/core/components/*.test  
        $ make runonly OnlyPlatforms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1
    2. Run the assets unit tests  
        $ cd /home/user/opencpi/projects/assets/components/\<sub-directory>/<.test>  
        $ make runonly OnlyPlatforms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1
    3. Run the assets_ts unit tests  
        $ cd /home/user/opencpi/projects/assets_ts/components/<.test>  
        $ make runonly OnlyPlatforms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1  
<br>

---------------------------------------------------------------------------------------------------
<a name="enable-opencpi-cards-&-slots"></a>

## Enable OpenCPI Cards & Slots
---------------------------------------------------------------------------------------------------
<a name="configure-hdl-platform-worker-with-slot"></a>

### Configure HDL Platform Worker with Slot
---------------------------------------------------------------------------------------------------
**CRITICAL NOTE:**  
- **Changes had to be made to the constaint file (listed below) in order to adapt the OpenCPI naming convention with the "Cononical Signal Name". This "Cononical Signal Name" convention can be seen in /home/user/opencpi/projects/core/hdl/cards/specs/fmc_hpc.xml. Wherever there are descrepancies between this naming convention and the naming convention used in the reference constraints file, it must be added within the slot definition of the zcu102.xml. For example:
~~~
    <slot name='FMC_HPC1' type='fmc_hpc'>
    <signal slot='LA01_P_CC'  platform='LA01_CC_P'/>
~~~
- **CODE BLOCK:**  
    -   **The "finalized" code block of various files mentioned in this section are located in the ocpi.osp.xilinx repository:**  
    - **ocpi.osp.xilinx/hdl/platforms/zcu102**  
        $ cd /home/user/opencpi/projects/osp/ocpi.osp.xilinx/hdl/platforms/zcu102

1. Edit the zcu102.xml to include the implementation of the FMC HPC Slot  
    1. Uncomment the following specproperty "nSlots". Update nSlots to have a value of 2, as there are two FMC_HPC slots on the zcu102, which will have the names FMC_HPC0 and FMC_HPC1
    2. Since the default slot names are acceptable, leave "slotNames" property commented out
    3. Uncomment the \<slot name='FMC_LPC' type='fmc_lpc'>
        1. Change the slot name from FMC_LPC to FMC_HPC0 (slot0). Once the FMC_HPC0 is fully developed, a copy will be made for defining FMC_HPC1 (slot1).
        2. Change the type from fmc_lpc to fmc_hpc        
    4. Remove all of the slot signals that were defined for the FMC_LPC slot definition, leaving an empty FMC_HPC slot definition. This will allow us to fill in the new FMC_HPC slot signals that are needed.
    5. The "Cononical Signal Name" that is described in the **CRITICAL NOTE** above needs to be implemented into **BOTH** of the slot definitions in the zcu102.xml. For example. As an example, since OpenCPI does not yet generate the correct stub driver for the FMC_HPCx_DPx and PG signals, these signals must be implemented into the zcu102.xml slot definitions as follows. (Reference the CODE BLOCK for the final solution)
        ~~~
        ...
        <signal slot='DP<#>_C2M_P' platform=''/>
        <signal slot='DP<#>_C2M_N' platform=''/>

        <signal slot='DP<#>_M2C_P' platform=''/>
        <signal slot='DP<#>_M2C_N' platform=''/>

        <signal slot='PG_C2M' platform=''/>
        <signal slot='PG_M2C' platform=''/>
        ~~~
    6. Copy the FMC_HPC0 (slot0) definition and create a duplicate HPC slot definition and rename the copy to be FMC_HPC1 (slot1).
    
2. Edit the zcu102.xdc to include the implementation of the two FMC HPC Slots. 
    1. Uncomment all of the FMC_HPC0_* and all of the FMC_HPC1_* signal names
    2. For each of the FMC_HPCx_DPx signals, an IOSTANDARD needs to be applied:
    ~~~
    set_property PACKAGE_PIN T1       [get_ports "FMC_HPC0_DP6_M2C_N"] ;# Bank 228 - MGTHRXN0_228
    set_property IOSTANDARD  LVCMOS18 [get_ports "FMC_HPC0_DP6_M2C_N"] ;# Bank 229 - MGTHRXN2_229
    ~~~
<br>

<a name="building-the-hdl-platform-worker-with-slot-enabled"></a>

### Building the HDL Platform Worker with Slot enabled
---------------------------------------------------------------------------------------------------
$ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
$ ocpidev clean  
$ ocpidev build --hdl-platform zcu102
<br>

<a name="install-and-deploy-the-slot-configuration"></a>

### Install and Deploy the Slot configuration
---------------------------------------------------------------------------------------------------
1. Install zcu102 (hdl-platform)  
    $ cd /home/opencpi  
    $ ocpiadmin install platform zcu102
2. Deploy: zcu102 with xilinx19_2_aarch64  
    $ ocpiadmin deploy platform xilinx19_2_aarch64 zcu102
<br>

<a name="cards-&-slots-verification:-fsk-modem-on-the-fmcomms2"></a>

### Cards & Slots verification: FSK modem on the fmcomms2
---------------------------------------------------------------------------------------------------
GOAL:
- Verify the FMC slot definitions in the HDL Platform Worker, by building the appropriate HDL assemblies/containers (i.e. fsk_modem), running the application (fsk_modem_app) and verifying the output file.
- A fmcomms2 daugthercard must be install in the appropriate slot (FMC_HPC0/1) of the zcu102 and its corresponding bitstream must be loaded in the FPGA.
- A new card specification is created as a work around to a limitation in OpenCPI. The limitation is that OpenCPI only supports inserting cards which implement the same type as the slot. But in reality, slots can support a super-set of the cards types. In this case, the fmcomms2 is a FMC_LPC, but the slot is a FMC_HPC. In this section, the new card is simply a copy of the fmcomms_2_3_lpc_scdcd.xml that is renamed to fmcomms_2_3_hpc_scdcd.xml and its type changed to "fmc_hpc".xml
- **NOTE: The [Verification test result table](#verification-test-result-table) section in the appendix contains the verfication test results of all zcu102 board variations.**

**CODE BLOCK:**  
**The "finalized" code block of various files mentioned in this section are located in the ocpi.osp.xilinx repository:**  
- **ocpi.osp.xilinx/hdl/assemblies/fsk_modem/** 

<a name="Building the fsk_modem HDL Assemblies/Containers"></a>

#### Building the fsk_modem HDL Assemblies/Containers
- **The fsk_modem hdl assembly is located in the following directory:**   
    - ocpi.osp.xilinx/hdl/assemblies/fsk_modem
- HDL Assembly XML
    - For better organization purposes, an HDL Assembly directory is created, named "fsk_modem", in this project to house a local copy of said assembly and bitstream. The HDL Assembly XML (fsk_modem.xml) from the assets/hdl/assemblies/fsk_modem/fsk_modem.xml is simply copied into this directory.
    - HDL Container XML and constraint files are created so that the fmcomms2 daughtercard is included within the build of the fsk_modem HDL Assembly. 
        - Once again, the zcu104 is used as a reference. Specifically, within the assets/hdl/assemblies/fsk_modem/, the zcu104 Container XML and constraints files will be relied upon for required content.
1. Make an HDL Assembly directory, name "fsk_modem" in this project  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx/  
    $ ocpidev create hdl assembly fsk_modem
2. Copy fsk_modem XML from assets/hdl/assemblies/fsk_modem into the newly created HDL Assembly folder  
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/fsk_modem.xml /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/assemblies/fsk_modem
3. Copy fsk_modem Makefile from assets/hdl/assemblies/fsk_modem into the newly created HDL Assembly  
    $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/Makefile /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/assemblies/fsk_modem  
4. $ cd /home/user/opencpi/projects/ocpi.osp.xilinx/hdl/assemblies/fsk_modem
5. Copy the zcu102 HDL platform constraint file into the newly created HDL assembly and rename it cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc
    - **CRITICAL NOTE: ONLY 1 constraints file is used for all Container builds**  
    $ cp /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/platforms/zcu102/zcu102.xdc /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/assemblies/fsk_modem/cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc
6. Edit newly created constraint file cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc in the following way:  
- **NOTE:** Given that the zcu102 has the ability to house two HPC (high-pin-count) daughter-boards, the constraints file contains coverage for both HPC slots, FMC_HPC0 and FMC_HPC1.  
- **NOTE** Two sections are appended to the original HDL platform constraints file code, one for each of the HPC slots that are defined in this section ("Slot HPC0", "Slot HPC1").  
- **NOTE:** Throughout this section be mindfull of the port names when implementing from one file and be sure to name each port FMC_HPC0_* or FMC_HPC1_* . When implementing any new ports from the following files, be sure to comment out any redundant ports from the original HDL platform constraint file portion as to make these new ports priority.  
    1. Define the constraints for the platform/card pairing
        1. To guarantee the correct constraints are used for the zcu102/fmcomms2 pairing, the Analog Devices reference design is leveraged by copying the constraints file into the cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc. 
            - The reference design is found at https://github.com/analogdevicesinc/hdl/blob/master/projects/fmcomms2/zcu102/system_constr.xdc.
            - As there are two FMC_HPC0/1 slots, two copies of these set of constraints are applied to the cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc, with the appropriate changes made to the pin assignments. But For now, only one copy is required.
        2. Update the port names to agree with the OpenCPI zcu102 Platform Worker XML and core/hdl/devices/specs/fmc_hpc.xml files, for example:  
            FROM: [get_ports rx_clk_in_p]  
            TO: [get_ports FMC_HPC0_LA00_CC_P]
        3. For now, do **NOT** include the 'create_clock -name rx_clk' signal
    2. In order to adhere to clock constraints determined for the zcu104/fmcomms2 pairing, copy all of the constraints relevant to the fmcomms2 from the assets/hdl/assemblies/fsk_modem/cnt_zcu104_fmcomms_2_3_scdcd.xdc file into the newly created cnt_zcu102_fmcomms_2_3_hpc_scdcd.xdc.
    3. Update all of the relevant constraints by ensuring that they have the FMC_HPC0* prefix.
    4. Implement support for the FMC_HPC1 slot
        1. Make a copy of the FMC_HPC0 constraints performmed in the steps above
        2. Changing the prefix on the copied constraints from FMC_HPC0 to FMC_HPC1
        3. Update the pin assignments for the FMC_HPC1 slot per the zcu102 FPGA connectivity to the FMC_HPC1 slot.
7. Create a new card specification (work around to FOSS limitation)
    1. Copy the fmcomms_2_3_lpc_scdcd.xml and rename it fmcomms_2_3_hpc_scdcd.xml
        $ cp /home/user/opencpi/projects/assets/hdl/cards/specs/fmcomms_2_3_lpc_scdcd.xml /home/user/opencpi/projects/assets/hdl/cards/specs/fmcomms_2_3_hpc_scdcd.xml
    2. $ cd /home/user/opencpi/projects/assets/hdl/cards/specs
    3. Edit the newly created fmcomms_2_3_hpc_scdcd.xml and change the 'card type' from "fmc_lpc" to "fmc_hpc"
        ~~~
        <card type="fmc_hpc">
        ~~~
    4. Update the /home/user/opencpi/projects/assets/hdl/cards/lib directory with new card specification, which makes it visible to the build system:  
        $ cd /home/user/opencpi/projects/assets/  
        $ make cleanexports  
        $ make exports
8. Create the "base" Container files for the FMC_HPC0 and FMC_HPC1 slots
    1. Make two copies of the zcu104 Containers files and edit for zcu102 (FMC_HPC0 and FMC_HPC1)
        - $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/cnt_zcu104_fmcomms_2_3_scdcd.xml /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/assemblies/fsk_modem/cnt_zcu102_base_hpc0_fmcomms_2_3_hpc_scdcd.xml
        - $ cp /home/user/opencpi/projects/assets/hdl/assemblies/fsk_modem/cnt_zcu104_fmcomms_2_3_scdcd.xml /home/user/opencpi/projects/osps/ocpi.osp.xilinx/hdl/assemblies/fsk_modem/cnt_zcu102_base_hpc1_fmcomms_2_3_hpc_scdcd.xml
    3. Edit Container XML per the following:
        1. Replace zcu104 with zcu102
        2. Update each "card" attribute to the new card specification
            - from "fmcomms_2_3_lpc_scdcd" to "fmcomms_2_3_hpc_scdcd"
        3. Update each "device" instance to ensure that the slot attribute is assigned to the appropriately, i.e. **slot='fmc_hpc0'** or **slot='fmc_hpc1'**
            ~~~
              <Device name='data_src_qadc0' card='fmcomms_2_3_hpc_scdcd' slot='fmc_hpc0'>
            ~~~
        4. Update each "connection" **made to/from the card**, such that the slot attribute is assigned appropriately, i.e. **slot='fmc_hpc0'** or **slot='fmc_hpc1'**
            ~~~
            <Connection external='in_to_asm_rx_path_from_adc'
            port='out'
            device='data_src_qadc0'
            card='fmcomms_2_3_hpc_scdcd'
            slot='fmc_hpc0'/>
            <Connection external='out_from_asm_tx_path_to_dac'
            port='out'
            device='data_src_qddc0'
            card='fmcomms_2_3_hpc_scdcd'
            slot='fmc_hpc0'/>
            ~~~
8. Edit the Makefile to include the newly created container files. 
9. Building the fsk_modem HDL Assembly and Containers (bitstreams)
    - This steps builds two bitreams for testing the fmcomms2 when install in each slot  
    $ cd /home/user/opencpi/projects/osps/ocpi.osp.xilinx  
    $ ocpidev build --hdl-assembly fsk_modem --hdl-platform zcu102  

<a name="running-the-fsk_modem_app"></a>

#### Running the fsk_modem_app
1. Ensure the VADJ jumper is configured for the specific zcu102 SOM
2. Install the fmcomms2 card into the FMC Slot0
3. Install a coax cable from the fmcomms2/TX1A to fmcomms2/RX2A
    - Yep it's TX1A-> RX2A.
4. Setup zcu102 for Network Mode ([Network Mode setup](#network-mode-setup))  
    (i.e. customize your `mynetsetup.sh` to nfs mount to your dev host cdk and the various projects containing necessary run-time artifacts)
    - Be sure that the `mynetsetup.sh` script has the following mount points that will now include the ocpi.osp.xilinx project
    ~~~
    mkdir -p /mnt/net                                      
    mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
    mkdir -p /mnt/ocpi_core
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
    mkdir -p /mnt/ocpi_assets
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
    mkdir -p /mnt/ocpi_assets_ts
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
    mkdir -p /mnt/ocpi_zcu102
    mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/osps/ocpi.osp.xilinx /mnt/ocpi_zcu102
    ~~~
5. Change directories to the fsk_dig_radio_ctrlr application:  
    % cd /mnt/ocpi_assets/applications/fsk_dig_radio_ctrlr  
6. Setup artifacts search path:  
    % export OCPI_LIBRARY_PATH=/mnt/ocpi_zcu102/hdl/assemblies/fsk_modem/container-fsk_modem_zcu102_base_cnt_zcu102_base_hpc0_fmcomms_2_3_hpc_scdcd:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts  
7. Disable the OCPI_DMA_CACHE_MODE which have shown issues with the zynq ultrascale+ family  
    % export OCPI_DMA_CACHE_MODE=0
8. Run app for enough time, -t, so that the entire data file (picture) can flow through the app  
    **CRITICAL NOTE: Intermittent issues have been seen when using network mode to run the fsk_modem_app.xml, if the picture below does not present itself, try to unload the fpga on the zcu102 by running $ ocpihdl unload. Once unloaded rerun the app (which will reload the fpga) and this should resolve the issues.**  
    % ocpirun -v -d -x -t 15 fsk_modem_app.xml  
9. On your develop host:
    - $ cd /home/user/opencpi/projects/assets/applications/fsk_dig_radio_ctrlr  
    - $ eog fsk_dig_radio_ctrlr_fmcomms_2_3_txrx.bin  
        - SUCCESS is defined as displaying the expected picture
10. Power down the zcu102 and move the fmcomms2 card from Slot0 to Slot1
    1. Repeat steps 2-5
    2. Setup artifacts search path to ensure the bitstream for Slot0 is selected:
    % export OCPI_LIBRARY_PATH=/mnt/ocpi_zcu102/hdl/assemblies/fsk_modem/container-fsk_modem_zcu102_base_cnt_zcu102_base_hpc1_fmcomms_2_3_hpc_scdcd:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts 
    3. Repeat steps 7-10
<br>

<a name="build-and-run-all-component-unit-tests-for-cards-&-slots"></a>

### Build and run all Component Unit Tests for Cards & Slots
---------------------------------------------------------------------------------------------------
GOAL: 
- Once Cards & Slots have been implemented into the design and the FSK application has been successfully executed, the user must now regression test using the framework provided Component Unit Tests. This will gives the user absolute certainty that the Cards & Slots design implementations have not interfered with performance of the system.
- The goal of this section is to clean/build and retest the Component Unit Tests that were built in the previous section. Success of this section is to perform all of the Component Unit Tests making sure that each of the tests "pass or fail" as they did in the previous section. Please refer to the [Component Unit Test result table](#component-unit-test-result-table) section to insure that testing behavior is consistent.
- **NOTE: The [Component Unit Test result table](#component-unit-test-result-table) section in the appendix contains the Component unit test results of all component unit tests for each of the zcu102 board variations.**

1. Clean all Component Unit Test bitstreams which were built prior to including the slot support  
    $ make cleantest
2. Build Component Unit Tests (in parallel) on Development Host
    1. Build the core unit tests. (May take several hours)  
        Open a new terminal window
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/core  
        $ ocpidev build test --hdl-platform zcu102
    2. Build the assets unit tests (May take several hours)  
        Open a new terminal window
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/assets  
        $ ocpidev build test --hdl-platform zcu102
    3. Build the assets_ts unit tests (May take several hours)  
        Open a new terminal window
        $ cd /home/user/opencpi  
        $ source cdk/opencpi-setup.sh -s  
        $ cd /home/user/opencpi/projects/assets_ts  
        $ ocpidev build test --hdl-platform zcu102
3. Run the Component Unit Tests (Sequentially)
- **NOTE: To ensure the best test coverage, this section is very manually intensive. The user must traverse into each \<unit>.test directory of each component library and perform "runonly" followed by "verify" of each of the tests. Some of these tests are known to fail. A chart is provided in the [Tables of results](#test-results-table) below that outlines the expected outcome for each of these tests (as of v2.1.0).**
- Chapter 13 of https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Component_Development_Guide.pdf, details the various "make" calls used in these steps
- [Server Mode setup](#server-mode-setup) was used to run the Component Unit Tests because it has proven to yield to most consistent results
    1. Run the core component unit tests
        $ cd /home/user/opencpi/projects/core/components/<.test>  
        $ make runonly OnlyPlatfoms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1  
    2. Run the assets component unit tests
        $ cd /home/user/opencpi/projects/assets/components/\<sub-directory>/<.test>  
        $ make runonly OnlyPlatfoms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1  
    3. Run the assets_ts component unit tests
        $ cd /home/user/opencpi/projects/assets_ts/components/<.test>  
        $ make runonly OnlyPlatfoms=zcu102  
        $ make verify OnlyPlatforms=zcu102 TestAccumulateErrors=1  
<br>

---------------------------------------------------------------------------------------------------
<a name="appendix"></a>

## **APPENDIX**
---------------------------------------------------------------------------------------------------
### Petalinux Build
---------------------------------------------------------------------------------------------------
GOALS:
- The following Petalinux reference guide describes the commands and build flow that will be utilized in this section. These steps will be revisted in three sections of this guide, and will allow consequent bitstreams to be "spot checked": https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug1144-petalinux-tools-reference-guide.pdf  
- Successful completion of this section is a bootable SD card image utilizing the Petalinux utility in conjunction with resources provided in each of the following sections:
    1. [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)
    2. [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)
    3. [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)

**CRITICAL NOTES:**
- **As you progress through the guide this section will be utilized to create a 'petalinux project' in 3 different directories, one for each of the sections mentioned above. This will give the user the flexability to move between sections if needed. These directories are provided in the [Preview of the concluding directory structure](#preview-of-the-concluding-directory-structure) section.** 
- **Before implementing the build flow below, take note of the two Petalinux Build subsections [zcu102 Ethernet Bug fix for PetaLinux](#zcu102-ethernet-bug-fix-for-petalinux), and [Static IP Address Setup](#static-ip-address-setup) to see if you would like to implement them into your petalinux-build flow.**
- **Each time you finish a petalinux build and want to incorporate changes into the ZynqReleases directory so that the deployment artifacts are accrately implemented, you must clean stale xilinx19_2_aarch64 data. This can be accomplished by reviewing the [Install and Deploy the HDL Platform with Control Plane enabled](#install-and-deploy-the-hdl-platform-with-control-plane-enabled) section. Step 2 'Clean stale data in xilinx19_2_aarch64 (rcc-platform)' followed by a reinstallation of the xilinx19_2_aarch64 rcc-platform will implement accurate boot artifacts during a deployment.**

1. Source Petalinux 2019.2  
    $ source /opt/pkg/petalinux/2019.2/settings.sh
2. Create a petalinux project directory depending on section  
    **CRITICAL NOTE: Depending on which section you are referencing you will create a petalinux project directory accordingly:**  
    [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)   
    $ petalinux-create -t project --template zynqMP --name "plx_zcu102_minimal"  
    [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)  
    $ petalinux-create -t project --template zynqMP --name "plx_zcu102_cp"  
    [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)  
    $ petalinux-create -t project --template zynqMP --name "plx_zcu102_dp"  
3. CD into the newly created petalinux project directory.
4. Import the Hardware Configuration that was exported from the Vivado project. This is the *.xsa file that was created during the  File → Export → Export Hardware step during any of the three given sections above.  
    $ petalinux-config --get-hw-description=../ps8_zcu102_\<section>  
    **CRITICAL NOTE: For the [Reference Design](#reference-design) section in the Appendix use the following petalinux-config command to point to the .xsa file in the vendor_reference_design_package/ directory:**  
    $ petalinux-config --get-hw-description=../vendor_reference_design_package/hdl/projects/fmcomms2/zcu102
5. Once the /misc/config System Configuration GUI is present in the terminal, continue with the default settings and press 'e' (EXIT) and 'y' (YES).
    - If you are presented with: "ERROR: Failed to generate meta-plnx-generated layer", this can be fixed by increasing the 'max_user_watches' as follows:  
    $ sudo sysctl -n -w fs.inotify.max_user_watches=524288
6. Build the project  
    $ petalinux-build  
7. Once the build is complete, move into the images/linux
8. Package the BOOT.BIN image  
    $ petalinux-package --boot --fsbl --fpga --u-boot --force
    1. There should now be a BOOT.BIN in the images/linux/ directory  
    **CRITICAL NOTE:**  
    **The use of "–fpga" is not required if the desire is to not program the FPGA from u-boot. If you are using the ['Preset' Board Design Files (BDF)](#'preset'-board-design-files-(bdf)) method the '--fpga' option should not be used, as it does not contain a constraint file. This is due to issues seen when using Server Mode. When a bitstream IS loaded and packed into the BOOT.BIN, it is not possible to setup Server Mode.**
9.  Place the BOOT.BIN and image.ub into a micro-SD card that is configured for FAT32 and has (1) partition. Be sure to move or remove the contents from within the micro-SD card to be sure that it is empty.  
    - Link to partition SD-card: https://ragnyll.gitlab.io/2018/05/22/format-a-sd-card-to-fat-32linux.html  
    $ cp BOOT.BIN image.ub /run/media/\<user>/\<sd-card>/  
    unmount SD-Card from computer  
10. Install the SD card into the zcu102, power on the zcu102, and open a serial connection to observe the petalinux boot process.  
    $ sudo screen /dev/ttyUSB0 115200  
    plx_zcu102_'stage' login: root  
    Password:  
    root@plx_zcu102_'stage':~  
<br> 

<a name="zcu102-ethernet-bug-fix-for-petalinux"></a>

#### zcu102 Ethernet Bug fix for PetaLinux
GOAL: 
- Fix the Ethernet port issue seen when attempting to configure the eth0 port of the zcu102 after a Petalinux Build.

During a typical petalinux configuration/build process there is an issue with petalinux accurately configuring the Ethernet port of the zcu102. This issue can be seen when trying to configure the eth0 port of the device. This section will address the issue in the petalinux build flow.

1. Issue:  
~~~
% ifconfig eth0 down
ifconfig: SIOCGIFFLAGS: No such device
~~~

2. Fix:  
$ cd /home/user/plx_zcu102_dp/project-spec/meta-user/recipes-bsp/device-tree/files  
edit system-user.dtsi  
~~~
/include/ "system-conf.dtsi"
/ {
};
&gem3
{
    phy-handle = <&phyc>;
    phyc: phy@c
    {
        reg = <0xc>;
        ti,rx-internal-delay = <0x8>;
        ti,tx-internal-delay = <0xa>;
        ti,fifo-depth = <0x1>;
        ti,rxctrl-strap-worka;
    };
};
~~~

3. Repeat Steps 4-10 in the [Petalinux Build](#petalinux-build) to implement the ethernet patch.
<br>

<a name="static-ip-address-setup"></a>

#### Static IP Address Setup
If your network is not setup for DHCP or you would rather use a Static-IP address for your embedded platform follow the steps below. This will allow you to establish a Static-IP address so that you do not have to manually reconfigure your ethernet connection upon each boot-cycle:

- This solution is outlined here:  
https://www.xilinx.com/support/answers/68614.html  
$ md5sum recipes-core.tar.gz  
b85347e66a345000ac9b9a4994525c02

1. The recipes-core.tar.gz and its contents are loacated within the ocpi.osp.xilinx repository in the following directory:  
    - \<path>/ocpi.osp.xilinx/guide/zcu102/Petalinux
2. CD into vivado project depending on section  
    **NOTE: Depending on which section you are referencing you will cd into ONE of the follow directories:**   
    $ cd ps8_zcu102_preset/   -->  [Identify minimal configuration of PS core IP](#identify-minimal-configuration-of-ps-core-ip)  
    $ cd ps8_zcu102_ocpi_cp/  -->  [Configure PS for OpenCPI HDL Control Plane](#configure-ps-for-opencpi-hdl-control-plane)  
    $ cd ps8_zcu102_ocpi_dp/  -->  [Configure PS for OpenCPI Data Plane](#configure-ps-for-opencpi-data-plane)
2. Copy the recipes-core directory to petalinux_project/project-spec/meta-user
3. Edit the interfaces file located below to the desired specifications:  
    - petalinux_project/project-spec/meta-user/recipes-core/init-ifupdown/init-ifupdown-1.0/interfaces
4. Repeat Steps 4-10 in the [Petalinux Build](#petalinux-build) to implement the Static-IP address changes.  
5. To verify that the changes have been successfully implemented perform the following command once you have logged into the device:
    $ ip a
    ~~~
    1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN group default qlen 1000
        link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
        link/ether 00:0a:35:00:1e:53 brd ff:ff:ff:ff:ff:ff
        inet 192.168.0.10/24 scope global eth0
           valid_lft forever preferred_lft forever
        inet6 fe80::20a:35ff:fe00:1e53/64 scope link 
           valid_lft forever preferred_lft forever
    3: sit0@NONE: <NOARP> mtu 1480 qdisc noop state DOWN group default qlen 1000
        link/sit 0.0.0.0 brd 0.0.0.0
    ~~~
    - Notice that the eth0 inet of 192.168.0.10 was established.  
<br>

<a name="reference-design"></a>

### Reference Design
---------------------------------------------------------------------------------------------------
GOALS:
- Begin with the zcu102 + fmcomms2 Reference Design that has been provided by Analog Devices (https://github.com/analogdevicesinc/hdl/tree/hdl_2019_r2), remove modules and ports which are not absolutely required for the PS to boot properly to identify the "minimal" PS core IP configuration that will support boot. This step is iterative, meaning that only a small change or removal should be made and then verified on the zcu102 to successfully boot the embedded oeprating system.
- The output of this section defines the "minimal" configuration that is Generated, HDL Wrapped and Exported for use in the Petalinux project that is used to create an SD-Card linux image which sucessfully boots on the target board.
- Once these steps are confirmed, then signals and ports (clock, reset, master (CP), slave (DP)) that are required for OpenCPI can be included, but this is detailed in a later step.

1. Locate and Build the Analog Devices zcu102 + fmcomms2 Reference Design
    1. Using the link provided above clone the repository into the /home/user/vendor_reference_design_package  
        $ cd /home/user/vendor_reference_design_package  
        $ git clone https://github.com/analogdevicesinc/hdl.git  
        $ cd /home/user/vendor_reference_design_package/hdl  
        $ git checkout -b hdl_2019_r2  
    2. Build the zcu102 + fmcomms2 Reference Design  
        $ cd /home/user/vendor_reference_design_package/hdl/projects/fmcomms2/zcu102  
        $ source /opt/Xilinx/Vivado/2019.2/settings64.sh  
        $ export ADI_IGNORE_VERSION_CHECK=1  
        $ make  

2. Modify the Reference Design to enable the "minimal" PS design.
    1. Open the fmcomms2_zcu102.xpr project  
        $ vivado fmcomms2_zcu102.xpr
    2. Open the Block Diagram of the project
    3. Remove modules and ports which are no absolutely required for the PS to boot properly. **Picture provided below**.
    4. Once the Block DDesign updates are complete,
        1. Select the Diagram tab, RMC Validate Design
        2. Select Generate Bitstream
    5. Once the bitstream has been generated, export the hardware by selecting:
        1. File -> Exports -> Export Hardware... -> Include Bitstream -> OK
    7. Return to the [Petalinux Build](#petalinux-build) section for the **plx_zcu102_minimal**, to update the SD card and confirm that the Embedded Linux will boot properly. For the petalinux build section keep in mind that the .xsa file that was generated in the hardware export is in the following directory structure: /home/user/vendor_reference_design_package/hdl/projects/fmcomms2/zcu102/system_top.xsa
    8. The design should look as follows:  
        ![Reference Design BD](./images/reference_design_BD.png)  
<br>

<a name="network-mode-setup"></a>

### Network Mode setup
---------------------------------------------------------------------------------------------------
GOAL: 
- The goal of this section is to enable the user with the ability to setup the Network Mode on the . Success of this section is the ability to source the customized `mynetsetup.sh` script that enables the Network Mode and provided the ability to load bitstreams from the Development Host (Computer) to the Platform Host ().

1. Establish a serial connection from the Host to the zcu102, open a terminal window:  
    $ sudo screen /dev/ttyUSB0 115200  
2. Petalinux Login:  
    plx_zcu102_dp login:root  
    Password:root  
3. Mount the OpenCPI filesystem
    \# mkdir opencpi  
    \# mount /run/media/mmcblk0p1/opencpi/ opencpi/  
    \# cd opencpi/  
4. Edit the default_mynetsetup.sh  
    1. Implement edits to reach the Development host opencpi directory  
        \# cp `default_mynetsetup.sh` ./mynetsetup.sh  
        \# vi `mynetsetup.sh`  
        Under the section '# Mount the opencpi development system as an NFS server, onto /mnt/net ...' Add the following lines which are necessary for mount the core, assets, and assets_ts build-in projects.  
        ~~~
        mkdir -p /mnt/net                                  
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
        
        mkdir -p /mnt/ocpi_core               
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        
        mkdir -p /mnt/ocpi_assets   
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets

        mkdir -p /mnt/ocpi_assets_ts
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
        ~~~
    2. Implement edits to automatically setup IP-Address  
        **NOTE: These commands can be done each time at boot-up, or be implemented into the `mynetsetup.sh`, if these commands do not work for your implementation consider manual entry**  
        % vi mynetsetup.sh  
        Under the commented section #Uncomment this section and change the MAC address for an enviornment with Multiple...  
        Do not uncomment that code, simply add the following code after that code-block.  
        ~~~
        ifconfig eth0 down
        ifconfig eth0 add <Valid ip-address> netmask 255.255.255.0
        ifconfig eth0 up
        ~~~
5. Source the `mynetsetup.sh` script to enable opencpi network mode  
    \# cd opencpi/  
    \# source /home/root/opencpi/mynetsetup.sh \<work station ip address> /home/user/opencpi  
    Example:  
    source /home/root/opencpi/mynetsetup.sh 192.168.0.100 /home/user/opencpi  
    ~~~
    root@my_petalinux_project:~/opencpi# source /home/root/opencpi/mynetsetup.sh 192.168.0.100 /home/user/opencpi
    macb e000b000.ethernet eth0: link down
    IPv6: ADDRCONF(NETDEV_UP): eth0: link is not ready
    An IP address was detected.
    My IP address is: 192.168.0.10, and my hostname is: my_petalinux_project
    macb e000b000.ethernet eth0: link up (1000/Full)
    IPv6: ADDRCONF(NETDEV_CHANGE): eth0: link becomes ready
    Attempting to set time from time.nist.gov
    rdate: bad address 'time.nist.gov'
    ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
    Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
    Running login script. OCPI_CDK_DIR is now /mnt/net/cdk.
    Executing /home/root/.profile
    No reserved DMA memory found on the linux boot command line.
    opencpi: loading out-of-tree module taints kernel.
    NET: Registered protocol family 12
    Driver loaded successfully.
    OpenCPI ready for zynq.
    Loading bitstream
    Bitstream successfully loaded
    Discovering available containers...
    Available containers:
     #  Model Platform            OS     OS-Version  Arch     Name
     0  hdl   zcu102                                 PL:0
     1  rcc   xilinx19_2_aarch64  linux  19_2        aarch32  rcc0
    ~~~
<br>

<a name="Platform Configuration XML"></a>

### Platform Configuration XML 
---------------------------------------------------------------------------------------------------
- Reference official OpenCPI documentation to understand the Platform Configuration capability.

The purpose of this section is to mention that a working example of using non-"base" Platform Configurations is included in the OSP. While they have been verified, they are not included by default during the build of the platform or target assembly.

- To include them, the user must edit the zcu102/Makefile and the fsk_modem/Makefile to include the following files, repectively:
    - The two Platform Configuration XML files provided with the zcu102 OSP are:
        - cfg_1rx_1tx_fmcomms_2_3_hpc0_lvds.xml
        -  cfg_1rx_1tx_fmcomms_2_3_hpc1_lvds.xml    
    - For verifying the Platform Configurations, two Container XML files are provided in the zcu102/hdl/assemblies/fsk_modem/ assembly directory
        - cnt_zcu102_1rx_1tx_hpc0_fmcomms_2_3_hpc_scdcd.xml
        - cnt_zcu102_1rx_1tx_hpc1_fmcomms_2_3_hpc_scdcd.xml
    - For test, repeat the steps in [Running the fsk_modem_app](#running-the-fsk_modem_app)  
    **CRITICAL NOTE: Ensure the OCPI_LIBRARY_PATH is set the target the exact bitstream per card/slot installation configuration**
        - % export OCPI_LIBRARY_PATH=/mnt/ocpi_zcu102/hdl/assemblies/fsk_modem/container-fsk_modem_zcu102_cfg_1rx_1tx_fmcomms_2_3_hpc0_lvds_cnt_zcu102_1rx_1tx_hpc0_fmcomms_2_3_hpc_scdcd:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
        - % export OCPI_LIBRARY_PATH=/mnt/ocpi_zcu102/hdl/assemblies/fsk_modem/container-fsk_modem_zcu102_cfg_1rx_1tx_fmcomms_2_3_hpc1_lvds_cnt_zcu102_1rx_1tx_hpc1_fmcomms_2_3_hpc_scdcd:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
<br>

<a name="Test result tables"></a>

### Test result tables 
---------------------------------------------------------------------------------------------------
**OpenCPI Version: v2.1.0**  
Date Tested: 04/14/2021
<br>

**TABLE KEY:**  
P  - Pass   
Case - Case Listed failed   
Excluded - Excluded due to issues seen within the framework   
<br>

<a name="Component Unit Test result table"></a>

#### **Component Unit Test result table**

| Component Library                 | zcu104        | zcu102        |
|-----------------------------------|---------------|---------------|
| **core/components**               |               |               |
| backpressure                      | P             | P             |
| bias                              | P             | P             |
| metadata_stressor                 | P             | P             |
| **assets/components/comms_comps** |               |               |
| mfsk_mapper                       | P             | P             |
| **assets/components/dsp_comps**   |               |               |
| cic_dec                           | P             | P             |
| cic_int                           | case01.05,    | case01.05,    |
|                                   | case01.10     | case01.10     |
| complex_mixer                     | P             | P             |
| dc_offset_filter                  | P             | P             |
| downsample_complex                | P             | P             |
| fir_complex_sse                   | P             | P             |
| fir_real_sse                      | P             | P             |
| iq_imbalance_fixer                | P             | P             |
| phase_to_amp_cordic               | P             | P             |
| pr_cordic                         | P             | P             |
| rp_cordic                         | P             | P             |
| **assets/components/misc_comps**  |               |               |
| cdc_bits_tester                   | Excluded      | Excluded      |
| cdc_count_up_tester               | Excluded      | Excluded      |
| cdc_fifo_tester                   | Excluded      | Excluded      |
| cdc_pulse_tester                  | Excluded      | Excluded      |
| cdc_single_bit_tester             | Excluded      | Excluded      |
| cswm_to_iqstream                  | P             | P             |
| data_src                          | P             | P             |
| delay                             | P             | P             |
| iqstream_to_cswm                  | P             | P             |
| iqstream_to_timeiq                | P             | P             |
| test_clock_generator              |               |               |
| timeiq_to_iqstream                | P             | P             |
| **assets/components/util_comps**  |               |               |
| agc_real                          | P             | P             |
| fifo                              | P             | P             |
| pattern_v2                        | P             | P             |
| test_tx_event                     | P             | P             |
| timestamper                       | P             | P             |
| timestamper_scdcd                 | P             | P             |
| zero_pad                          | P             | P             |
| **assets_ts/components**          |               |               |
| cic_dec_ts                        | P             | P             |
| complex_mixer_ts                  | Excluded      | Excluded      |
| dc_offset_filter_ts               | P             | P             |
| downsample_complex_ts             | P             | P             |
| fir_complex_sse_ts                | P             | P             |
<br>

<a name="Verification test result table"></a>

#### **Verification test result table**

| Verification test                 | zcu104        | zcu102        |
|-----------------------------------|---------------|---------------|
| pattern_capture                   | P             | P             |
| testbias                          | P             | P             |
| fsk/file_rw                       | P             | P             |
| fsk_drc/fsk_modem_app             | P             | P             |
<br>

